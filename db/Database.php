<?php

namespace db;

use PDO;

class Database extends AbstractDatabase {

    protected $tableName;
    private static $_instance;

    private function __construct() {
        $this->dsn = "mysql:"
                . "dbname=" . $this->getDbname() . ";"
                . "host=" . $this->getHost() . ";"
                . "charset=" . $this->getCharset();
//        $this->user = $this->getUser();
//        $this->password = $this->getPassword();

        try {
            $DBH = new PDO($this->getDsn(), $this->getUser(), $this->getPassword());
            $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//            $DBH->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
            $this->connect = $DBH;
//            echo 'Connected<br>';
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
            exit;
        }
    }

    private function __clone() {
        
    }

    public static function instance() {
        if (!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function connect() {
        return $this->connect;
    }

    public function close() {
        $this->connect = null;
    }

    public function query($query) {
        try {
            $result = $this->connect->query($query);
            $resultArray = array();
            if ($result != false) {
                foreach ($result as $value) {
                    $resultArray[] = $value;
                }
            }
            return $resultArray;
        } catch (\PDOException $ex) {
//            sout($ex);
//            if ($ex->errorInfo[0] != "HY000") {
//                sout("DATABASE exception:");
//                sout($ex);
//            }
//
//            if (strpos($query, "SELECT id FROM category_from_csv WHERE full_code") !== false) {
//                sout("DATABASE PRODUCT_CLASS_QUERY_EXCEPTION:");
//                sout($query);
//                sout($ex);
//                
//            }
//            
            return array();
        }
    }

    public function findOne($query, $key) {
        $res = $this->connect->query($query);
        if ($res != FALSE) {
            foreach ($res as $value) {
                return($value[$key]);
            }
        }
        return "";
    }

    public function insert($data) {
        $tableName = $this->getTableName();
        $queryHead = "INSERT INTO $tableName";
        $columns = " (";
        $values = " VALUES (";


        foreach ($data as $key => $value) {

            $columns .= "$tableName.$key, ";
            $values .= ":$key, ";

//            sout($value);
        }
        $columns = rtrim($columns, ', ');
        $columns .= ") ";
        $values = rtrim($values, ', ');
        $values .= ")";

        $query = $queryHead . $columns . $values;
        $stmt = $this->connect->prepare($query);
        $stmt->execute($data);
        return $this->connect->lastInsertId();
    }

    public function update2($data) {
        $tableName = $this->getTableName();
        $queryHead = "UPDATE $tableName ";
        $where = " WHERE id=:id";
        $set = "SET ";
        $query = "";
        $queryData = array();
        foreach ($data as $key => $value) {
            if ($key != "id") {
                $set .= "$tableName.$key=:$key, ";
            }
            $queryData[":$key"] = $value;
        }

        $set = rtrim($set, ', ');
        $query .= $set . $where;

        $query = $queryHead . $set . $where;
        $stmt = $this->connect->prepare($query);
        $stmt->execute($queryData);
    }

    public function update($data, $where) {
        $query = "UPDATE " . $this->tableName . " ";
        $set = "SET ";
        $column = "";
        foreach ($data as $key => $value) {
            if (strpos($value, "'") !== false) {
                $value = str_replace("'", "\'", $value);
            }
            $column .= $key . "='" . $value . "', ";
        }
        $set .= $column;
        $set = substr($set, 0, -2);
        $set .= " ";
        $query .= $set . $where;

        $succes = $this->query($query);
        return $succes;
    }

    public function getAllAsArray() {
        $query = "SELECT * FROM " . $this->getTableName();
        $res = $this->query($query);
        $result = array();
        foreach ($res as $value) {
            $result[] = $value;
        }
        return $result;
    }

    public function truncate($tableName = "") {
        if ($tableName == "") {
            $tableName = $this->getTableName();
        }
        $query = "TRUNCATE " . $tableName;
        $this->query($query);
    }

    function getTableName() {
        return $this->tableName;
    }

    function setTableName($tableName) {
        $this->tableName = $tableName;
    }

    public function isExist($query) {

        $result = $this->query($query);
        if (count($result) != 0) {
            return true;
        }

        return false;
    }

}
