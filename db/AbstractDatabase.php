<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace db;

/**
 * Description of AbstractDatabase
 *
 * @author tamas
 */
abstract class AbstractDatabase {

    protected $dsn;
    protected $dbname = SR_DB_NAME;
    protected $host = DB_HOST;
    protected $charset = "utf8";
    protected $user = DB_USER;
    protected $password = DB_PASSWORD;
    protected $connect;
    protected $query;
    protected $data;

    abstract function query($query);

//    abstract function findOneByQuery($query, $key);
    
    abstract function findOne($query, $key);

    abstract function connect();

    abstract function close();

    function getDsn() {
        return $this->dsn;
    }

    function getDbname() {
        return $this->dbname;
    }

    function getHost() {
        return $this->host;
    }

    function getCharset() {
        return $this->charset;
    }

    function getUser() {
        return $this->user;
    }

    function getPassword() {
        return $this->password;
    }

    function getGetConnect() {
        return $this->connect;
    }

    function getQuery() {
        return $this->query;
    }

    function setDsn($dsn) {
        $this->dsn = $dsn;
    }

    function setDbname($dbname) {
        $this->dbname = $dbname;
    }

    function setHost($host) {
        $this->host = $host;
    }

    function setCharset($charset) {
        $this->charset = $charset;
    }

    function setUser($user) {
        $this->user = $user;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setConnect($connect) {
        $this->connect = $connect;
    }

    function setQuery($query) {
        $this->query = $query;
    }

}
