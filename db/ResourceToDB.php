<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace db;

/**
 * Description of ResourceToDB
 *
 * @author szekus
 */
class ResourceToDB {

    private $db;
    private $resource;
    private $tableName;
    private static $prefix = "aaasr_";

    public static function create(\resources\Resource $resource) {
        $resourceToDB = new ResourceToDB();
        $apiEndpoint = $resource->getApiEndpoint();
        $tableName = substr($apiEndpoint, 1);
        $resourceToDB->tableName = self::$prefix . $tableName;
        return $resourceToDB;
    }

    private function __construct() {
//        $db = Database::instance();
//        $db->connect();
        $this->db = Database::instance();
//        $this->db->connect();
    }

    function getTableName() {
        return $this->tableName;
    }

    function setTableName($tableName) {
        $this->tableName = $tableName;
    }

    public function getAllResource() {
        $query = "SELECT * FROM " . $this->getTableName();
        $res = $this->db->query($query);
        $result = array();
        foreach ($res as $val) {
            $result[] = $val;
        }
        return $result;
    }

    public function updateToDB($resourceDatas) {
        $this->updateToDB2($resourceDatas);
//        $queryHead = "UPDATE " . $this->getTableName() . " ";
//        foreach ($resourceDatas as $resourceData) {
//            $firtsElemt = true;
//            $where = " WHERE ";
//            $set = "SET ";
//            $query = "";
//            foreach ($resourceData->getData() as $key => $value) {
//
//                $val = $value;
//
//                if (is_array($value)) {
//                    $val = $value["href"];
//                }
//
//                if ($firtsElemt) {
//                    $where .= "id = '$val'";
//                    $firtsElemt = false;
//                } else {
//                    if (strpos($val, "'") !== false) {
//                        $val = str_replace("'", "\'", $val);
////                        sout("BUG");
////                        sout($val);
//                    }
//                    $column = $this->getTableName() . "." . $key . "='" . $val . "', ";
//                    $set .= $column;
//                }
//            }
//            $set = substr($set, 0, -2);
//            $set .= " ";
//            $query .= $set . $where;
//
//
//            $query = $queryHead . $set . $where;
//            sout($query);
//            $succes = $this->db->query($query);
//        }
    }

    public function updateToDB2($resourceDatas) {
        $tableName = $this->getTableName();
        $queryHead = "UPDATE $tableName ";
        foreach ($resourceDatas as $resourceData) {
            $where = " WHERE id=:id";
            $set = "SET ";
            $query = "";
            $data = array();
            foreach ($resourceData->getData() as $key => $value) {


                if (is_array($value)) {
                    $data[$key] = $value["href"];
                } else {
                    $data[$key] = $value;
                }

                if ($key != "id") {
                    $set .= "$tableName.$key=:$key, ";
                }
            }

            $set = rtrim($set, ', ');
            $query .= $set . $where;


            $query = $queryHead . $set . $where;
//            sout($query);
            $stmt = $this->db->getGetConnect()->prepare($query);
            $stmt->execute($data);
        }
    }

    public function insertToDB($resourceDatas) {


        $this->insertToDB2($resourceDatas);

//        $queryHead = "INSERT INTO " . $this->getTableName();
//
//
//        foreach ($resourceDatas as $resourceData) {
//            $query = "";
//            $queryVal = " VALUES(";
//            foreach ($resourceData->getData() as $key => $value) {
//
//                if (is_array($value)) {
//                    $queryVal .= "'" . $value["href"] . "', ";
//                } else {
//                    if (strpos($value, "'") !== false) {
//                        $value = str_replace("'", "\'", $value);
//                        sout("BUG => '");
//                        sout($value);
//                    }
//                    $queryVal .= "'" . $value . "', ";
//                }
//            }
//            $queryVal = substr($queryVal, 0, -2);
//            $queryVal .= "), ";
//            $queryValues = $queryVal;
//            $queryValues = substr($queryValues, 0, -2);
////            $query = $queryHead . $queryCol . $queryValues;
//            $query = $queryHead . $queryValues;
//
//
//            $succes = $this->db->query($query);
//        }
    }

    public function insertToDB2($resourceDatas) {
        $tableName = $this->getTableName();

        foreach ($resourceDatas as $resourceData) {
            $queryHead = "INSERT INTO $tableName";
            $columns = " (";
            $values = " VALUES (";
            $data = array();
            $i = 0;
            foreach ($resourceData->getData() as $key => $value) {

                if (is_array($value)) {
                    $data[":" . $key] = $value["href"];
                } else {
                    $data[":" . $key] = $value;
                }
                $columns .= "`$key`, ";
                $values .= ":$key, ";
                $i++;
            }

            $columns = rtrim($columns, ', ');
            $columns .= ") ";
            $values = rtrim($values, ', ');
            $values .= ")";

            $query = $queryHead . $columns . $values;
            $stmt = $this->db->getGetConnect()->prepare($query);
            $stmt->execute($data);
        }
    }

    public function insertOrUpdateToDB($array) {
        if (count($array) > 0) {
            $this->deleteDeletedResource($array);

            foreach ($array as $resource) {
                $id = $resource->getData()["id"];

                $query = "SELECT * FROM $this->tableName WHERE id = '$id'";

                $result = $this->db->query($query);

                if (count($result) > 0) {
                    $updateArray = array();
                    $updateArray[] = $resource;
                    $this->updateToDB($updateArray);
                } else {
                    $insertArray = array();
                    $insertArray[] = $resource;
                    $this->insertToDB($insertArray);
                }
            }
        }
    }

    public function deleteDeletedResource($array) {
        $query = "SELECT * FROM $this->tableName";
        $result = $this->db->query($query);
        $oldData = array();
        foreach ($result as $value) {
            $oldData[] = $value["id"];
        }

        $newData = array();
        foreach ($array as $value) {
            $newData[] = $value->getData()["id"];
        }

        $deleteIds = array_diff($oldData, $newData);

        foreach ($deleteIds as $deleteId) {
            $query = "DELETE FROM $this->tableName WHERE id = '$deleteId'";
            $this->db->query($query);
        }
    }

    public function createDBTable($resource) {
        $query = "SELECT 1 FROM " . $this->getTableName() . " LIMIT 1";
//        sout($query);
        $res = $this->db->query($query);
//        sout($res);
        if ($res == false) {
            $query = "CREATE TABLE " . $this->getTableName() . " ( \n";
            $primaryKeyText = " NOT NULL PRIMARY KEY";
            foreach ($resource->getDataColumns() as $value) {

                if ($value == "id") {
                    $query .= $value . " varchar(100) NOT NULL PRIMARY KEY, \n";
                } else {
                    $query .= "`" . $value . "`" . " TEXT, \n";
                }
            }
            $query = substr($query, 0, -3);
            $query .= ")";

            $this->db->query($query);
        } else {
            $query = "DROP TABLE " . $this->getTableName();
            $this->db->query($query);
//            sout("BUG");
            $this->createDBTable($resource);
        }
    }

    public function truncateTable() {
        $query = "TRUNCATE $this->tableName";
        $this->db->query($query);
    }

    function getDb() {
        return $this->db;
    }

    function setDb($db) {
        $this->db = $db;
    }

}
