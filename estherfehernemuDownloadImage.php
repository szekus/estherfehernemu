<?php

require_once('loader.php');




$db = \db\Database::instance();

$images = array();

$query = 'SELECT * FROM v5qc2_virtuemart_medias';

$media_images = $db->query($query);
foreach ($media_images as $value) {
    $images[] = $value["file_url"];
}
sout($query);

$count = 1;
$max = count($images);
foreach ($images as $image) {
    $fileName = basename($image);
    $mainDir = "download/";


    $productImageDirPathArray = explode("/", $image);

    $makingDir = $mainDir;

    foreach ($productImageDirPathArray as $dir) {

        if ($dir === end($productImageDirPathArray)) {
            continue;
        }

        $makingDir .= $dir . "/";
        if (!file_exists($makingDir)) {
            mkdir($makingDir, 0777, true);
        }
    }

    $imgUrl = SHOP_IMAGE_BASE_URL . urlencodeWithoutBackSlash($image);
//    sout($imgUrl);
    file_put_contents($makingDir . $fileName, fopen($imgUrl, 'r'));

    sout($max . "/" . $count++);
}

function urlencodeWithoutBackSlash($url) {
    $array = explode('/', $url);
    $newUrl = "";
    for ($i = 0; $i < count($array); $i++) {
        $newUrl .= rawurlencode($array[$i]) . "/";
    }
    $newUrl = substr($newUrl, 0, -1);
    return $newUrl;
}
