<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of CouponCategoryRelation
 *
 * @author szekus
 */
class CouponCategoryRelation extends Resource {

    public static function create() {
        $resource = new CouponCategoryRelation();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    protected function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/couponCategoryRelations";
        $this->dataColumns = IResource::COUPON_CATEGORY_RELATION_ARRAY;
    }

}
