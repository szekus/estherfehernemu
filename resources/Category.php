<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of Category
 *
 * @author tamas
 */
class Category extends Resource {

    public static function create() {
        $resource = new Category();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/categories";
        $this->dataColumns = IResource::CATEGORY_ARRAY;
    }

    public function createSRId($categoryId) {
        return base64_encode("category-category_id=" . $categoryId);
    }

}
