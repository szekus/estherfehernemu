<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 *
 * @author tamas
 */
interface IResource {

    const ADDRESS_ARRAY = array("id", "innerId", "company", "firstname", "lastname", "taxNumber", "address1", "address2",
        "postcode", "city", "dateCreated", "dateUpdated", "customer", "country", "zone");
    const ATTRIBUTE_DESCRIPTION_ARRAY = array("id", "name", "description", "prefix", "postfix", "attribute", "language");
    const CATEGORY_ARRAY = array("id", "innerId", "picture", "sortOrder", "status", "productsStatus",
        "parentCategory", "centralCategory", "categoryDescriptions", "customerGroups", "categoryCustomerGroupRelations");
    const CATEGORY_DESCRIPTION_ARRAY = array("id", "name", "metaKeywords", "metaDescription", "description",
        "robotsMetaTag", "shortDescription", "category", "language");
    const COUNTRY_ARRAY = array("id", "name", "isoCode2", "isoCode3", "status", "zones");
    const COUPON_ARRAY = array("id", "code", "discountType", "percentDiscountValue", "loginRequired", "freeShipping",
        "fixDiscountValue", "dateStart", "dateEnd", "totalNumberOfCoupons", "totalNumberOfCouponsPerCustomer", "status",
        "dateCreated", "dateUpdated", "email", "minOrderLimit", "targetType", "taxClass", "couponDescriptions");
    const COUPON_CATEGORY_RELATION_ARRAY = array("id", "coupon", "category");
    const COUPON_DESCRIPTION_ARRAY = array("id", "name", "description", "coupon", "language");
    const COUPON_PRODUCT_RELATION_ARRAY = array("id", "coupon", "product",);
    const CURRENCY_ARRAY = array("id", "name", "code", "symbolLeft", "symbolRight", "decimalPlace", "value", "status", "dateUpdated");
    const CUSTOMER_ARRAY = array("id", "innerId", "firstname", "lastname", "email", "telephone", "fax", "password", "newsletter",
        "status", "approved", "freeShipping", "dateCreated", "dateUpdated", "defaultAdresses", "customerGroup", "loyaltyPoints", "addresses");
    const CUSTOMER_GROUP_ARRAY = array("id", "name", "percentDiscount", "percentDiscountSpecialPrices");
    const CUSTOMER_GROUP_PRODUCT_ARRAY = array("id", "customerGroup", "product", "price");
    const LENGTH_CLASS_ARRAY = array("id", "value");
    const LIST_ATTRIBUTE_ARRAY = array("id", "type", "name", "priority", "sortOrder", "required", "presentation",
        "defaultListAttributeValue", "listAttributeWidget", "attributeDescriptions", "listAttributeValues");
    const LIST_ATTRIBUTE_VALUE_ARRAY = array("id", "listAttribute", "listAttributeValueDescriptions");
    const LIST_ATTRIBUTE_VALUE_DESCRIPTION_ARRAY = array("id", "name", "listAttributeValue", "language");
    const LIST_ATTRIBUTE_VALUE_RELATION_ARRAY = array("id", "product", "listAttributeValue");
    const LOYALTY_POINT_ARRAY = array("id", "dateAdded", "dateExpiration", "value", "description", "order", "customer");
    const LOYALTY_POINT_USED_ARRAY = array("id", "dateUsed", "value", "description", "loyaltyPoint", "customer", "order");
    const MANUFACTURER_ARRAY = array("id", "innerId", "name", "picture", "sortOrder", "robotsMetaTag", "manufacturerDescriptions");
    const MANUFACTURER_DESCRIPTION_ARRAY = array("id", "description", "metaDescription", "metaKeywords", "manufacturer", "language");
    const ORDER_ARRAY = array("id", "innerId", "invoiceId", "invoicePrefix", "firstname",
        "lastname", "phone", "fax", "email", "shippingFirstname", "shippingLastname", "shippingCompany", "shippingAddress1",
        "shippingAddress2", "shippingCity", "shippingPostcode", "shippingCountryName", "shippingZoneName",
        "shippingAddressFormat", "shippingMethodName", "shippingMethodTaxRate", "shippingMethodTaxName", "shippingMethodExtension",
        "paymentFirstname", "paymentLastname", "paymentCompany",
        "paymentAddress1", "paymentAddress2", "paymentCity", "paymentPostcode", "paymentCountryName", "paymentZoneName",
        "paymentAddressFormat", "paymentMethodName", "paymentMethodCode", "paymentMethodTaxRate", "paymentMethodTaxName",
        "paymentMethodAfter", "taxNumber", "comment", "total", "value", "couponTaxRate", "dateCreated", "dateUpdated", "ip", "pickPackPontShopCode",
        "customer", "customerGroup", "shippingCountry", "shippingZone", "paymentCountry", "paymentZone", "orderStatus",
        "language", "currency", "orderTotals", "orderProducts");
    const ORDER_PRODUCT_ARRAY = array("id", "name", "sku", "modelNumber", "price", "total", "taxRate", "stock1", "stock2",
        "subtractStock", "idorder", "dateCreated", "dateUpdated", "order", "product", "orderProductOptions");
    const ORDER_STATUS_ARRAY = array("id", "orderStatusDescriptions");
    const ORDER_PRODUCT_OPTION_ARRAY = array("id", "name", "valueName", "price", "prefix", "orderProduct", "productOptionValue");
    const ORDER_STATUS_DESCRITION_ARRAY = array("id", "name", "color", "orderStatus", "language");
    const ORDER_TOTAL_ARRAY = array("id", "name", "valueText", "value", "sortOrder", "type", "key",
        "description", "dateCreated", "dateUpdated", "order");
    const PRODUCT_ARRAY = array("id", "innerId", "sku", "modelNumber", "orderable", "price", "multiplier",
        "multiplierLock", "loyaltyPoints", "stock1", "stock2", "subtractStock", "mainPicture", "width",
        "height", "length", "weight", "status", "imageAlt", "shipped", "minimalOrderNumber", "maximalOrderNumber",
        "minimalOrderNumberMultiply", "availableDate", "quantity", "sortOrder", "freeShipping", "dateCreated", "dateUpdated", "taxClass",
        "noStockStatus", "inStockStatus", "onlyStock1Status", "onlyStock2Status", "productClass", "parentProduct",
        "volumeUnit", "weightUnit", "manufacturer", "productDescriptions", "productSpecials", "productCategoryRelations",
        "relatedProducts", "productRelatedProductRelations", "productListAttributeValueRelations", "numberAttributeValues",
        "textAttributeValues", "productCollateralProductRelations", "collateralProducts");
    const PRODUCT_CATEGORY_RELATION_ARRAY = array("id", "product", "category");
    const PRODUCT_CLASS_ARRAY = array("id", "name", "description", "firstVariantSelectType",
        "secondVariantSelectType", "firstVariantParameter", "secondVariantParameter", "productClassAttributeRelations");
    const PRODUCT_CLASS_ATTRIBUTE_ARRAY = array("id", "productClass", "attribute");
    const PRODUCT_COLLATERAL_PRODUCT_RELATION_ARRAY = array("id", "product", "collateralProduct");
    const PRODUCT_DESCRIPTION_ARRAY = array("id", "name", "metaKeywords", "metaDescription", "shortDescription", "description",
        "customContentTitle", "customContent", "parameters", "packagingUnit", "measurementUnit", "videoCode", "product", "language");
    const PRODUCT_IMAGES_ARRAY = array("id", "imagePath", "imageAlt", "product");
    const PRODUCT_OPTION_ARRAY = array("id", "sortOrder", "product", "productOptionDescriptions", "productOptionValues");
    const PRODUCT_OPTION_DESCRIPTION_ARRAY = array("id", "name", "productOption", "language");
    const PRODUCT_OPTION_VALUE_ARRAY = array("id", "price", "prefix", "sortOrder", "productOption", "productOptionValueDescriptions");
    const PRODUCT_OPTION_VALUE_DESCRIPTION_ARRAY = array("id", "name", "productOptionValue", "language");
    const PRODUCT_RELATED_PRODUCT_RELATION_ARRAY = array("id", "product", "relatedProduct");
    const PRODUCT_PRODUCT_BADGE_RELATION_ARRAY = array("id", "product", "productBadge", "language");
    const PRODUCT_SPECIAL_ARRAY = array("id", "priority", "price", "dateFrom", "dateTo", "minQuantity", "maxQuantity",
        "product", "customerGroup");
    const PRODUCT_TAG_ARRAY = array("id", "product", "language", "tags");
    const STOCK_STATUS_ARRAY = array("id", "stockStatusDescriptions");
    const STOCK_STATUS_DESCRIPTION_ARRAY = array("id", "name", "color", "stockStatus", "language");
    const TAX_CLASS_ARRAY = array("id", "name", "description", "dateCreated", "dateUpdated", "taxRates");
    const URLALIAS_ARRAY = array("id", "type", "urlAliasEntity", "urlAlias");
    const WEIGHT_CLASS_ARRAY = array("id", "value");
    const ZONE_ARRAY = array("id", "name", "code", "status", "country");

}
