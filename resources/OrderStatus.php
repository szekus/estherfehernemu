<?php

namespace resources;

class OrderStatus extends Resource implements IResource {

    public static function create() {
        $resource = new OrderStatus();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    protected function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/orderStatuses";
        $this->dataColumns = IResource::ORDER_STATUS_ARRAY;
    }

   

    public function createOrderStausSelectArray() {
        $orderStatuses = $this->getAllFromDB();
        $select = array();
        foreach ($orderStatuses as $orderStatus) {
            $orderStatusHref = SR_APIURL . $this->getApiEndpoint() . "/" . $orderStatus->id;
            $query = "SELECT name FROM orderstatusdescriptions WHERE orderStatus = '$orderStatusHref' LIMIT 1";
            $name = $this->resourceToDB->getDb()->findOne($query, "name");
            $array = array();
            $array["id"] = $orderStatus->id;
            $array["name"] = $name;
            $select[] = $array;
        }

        return $select;
    }

}
