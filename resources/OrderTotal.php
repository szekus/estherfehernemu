<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of OrderTotal
 *
 * @author tamas
 */
class OrderTotal extends Resource implements IResource {

    public static function create() {
        $resource = new OrderTotal();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    protected function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/orderTotals";
        $this->dataColumns = IResource::ORDER_TOTAL_ARRAY;
    }

}
