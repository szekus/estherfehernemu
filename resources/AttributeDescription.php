<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of AttributeDescription
 *
 * @author szekus
 */
class AttributeDescription extends Resource {

    public static function create() {
        $resource = new AttributeDescription();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/attributeDescriptions";
        $this->dataColumns = IResource::ATTRIBUTE_DESCRIPTION_ARRAY;
    }

}
