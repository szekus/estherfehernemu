<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of Zone
 *
 * @author tamas
 */
class Zone extends Resource {

    public static function create() {
        $resource = new Zone();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/zones";
        $this->dataColumns = IResource::ZONE_ARRAY;
    }

    public function getAsArray() {
        return array_merge(parent::getAsArray(), get_object_vars($this));
    }

}
