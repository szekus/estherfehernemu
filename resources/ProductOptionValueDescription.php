<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductOptionValueDescription
 *
 * @author szekus
 */
class ProductOptionValueDescription extends Resource {

    public static function create() {
        $resource = new ProductOptionValueDescription();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/productOptionValueDescriptions";
        $this->dataColumns = IResource::PRODUCT_OPTION_VALUE_DESCRIPTION_ARRAY;
    }

}
