<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductRelatedProductRelation
 *
 * @author tamas
 */
class ProductRelatedProductRelation extends Resource{
    public static function create() {
        $resource = new ProductRelatedProductRelation();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        $resource->setUp();
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/productRelatedProductRelations";
        $this->dataColumns = IResource::PRODUCT_RELATED_PRODUCT_RELATION_ARRAY;
    }
}
