<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

use Exception;

/**
 * Description of Product
 *
 * @author tamas
 */
class Product extends Resource {

    public function createProductCSV() {
        $result = querySRApi($this->apiEndpoint, [], "GET", "responseBody", false);
        $last = $result["last"]["href"];
        $parts = parse_url($last);
        parse_str($parts['query'], $query);
        $lastPage = $query['page'];

        $csvArray = array();
//        $lastPage = 1;
        for ($i = 0; $i <= $lastPage; $i++) {

            $res = querySRApi($this->apiEndpoint . "?page=" . $i, [], "GET", "responseBody", false);
            foreach ($res["items"] as $item) {
                $csv = array();
                $id = getId($item["href"]);
                $resultProduct = querySRApi($this->apiEndpoint . "/" . $id, [], "GET", "responseBody", false);
//                sout($resultProduct);
                $csv["id"] = $resultProduct["id"];
                $csv["innerId"] = $resultProduct["innerId"];
                $csv["sku"] = $resultProduct["sku"];
                $csv["orderable"] = $resultProduct["orderable"];
                $csv["stock1"] = $resultProduct["stock1"];
                $csv["stock2"] = $resultProduct["stock2"];
                $csv["stock3"] = $resultProduct["stock3"];
                $csv["stock4"] = $resultProduct["stock4"];
                $csv["subtractStock"] = $resultProduct["subtractStock"];
                $csv["status"] = $resultProduct["status"];
                $csv["quantity"] = $resultProduct["quantity"];
                $csvArray[] = $csv;
            }
        }
        $headers = array("id", "innerId", "sku", "orderable", "stock1", "stock2", "stock3", "stock4", "subtractStock", "status", "quantity");
        $fp = fopen('data/srProducts.csv', 'w');
        fputcsv($fp, $headers, ";");
        foreach ($csvArray as $fields) {
            fputcsv($fp, array_values($fields), ";");
        }
        fclose($fp);
    }

    public function getProductIdBySku($sku) {
        $sku = urlencode($sku);


        $result = querySRApi($this->apiEndpoint . "?sku=" . $sku, [], "GET", "responseBody", false);

        if (isset($result["items"])) {
//            sout($sku);
            $url = $result["items"][0]["href"];
            $id = $this->getUrlId($url);
            $res = querySRApi($this->apiEndpoint . "/" . $id, [], "GET", "responseBody", false);
            if (!is_array($res) && !array_key_exists('id', $res)) {
                sout("nincs sku getProductIdBySku(): " . $sku);
            }
            return $res["id"];
        } else {
            sout("nincs sku: ");
            sout($sku);
            sout($result);
            return "NO_PRODUCT";
        }
    }

    public function getProductSkuBySRId($SRId) {
        $result = querySRApi($this->apiEndpoint . "/" . $SRId, [], "GET", "responseBody", false);
        if (key_exists("id", $result)) {
            return $result["sku"];
        } else {
            sout("nincs termek: ");
            sout($SRId);
            sout($result);
            return "NO_PRODUCT";
        }
    }

    public static function create() {
        $resource = new Product();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
//        $resource->setUp();
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/products";
        $this->dataColumns = IResource::PRODUCT_ARRAY;
    }

    public function createSRId($productId) {
        return base64_encode("product-product_id=" . $productId);
    }

    public function getBySku($sku, &$counnter = 0) {
        $counnter++;
        $sku = urlencode($sku);

        $result = querySRApi($this->apiEndpoint . "?sku=" . $sku, [], "GET", "responseBody", false);
        if (is_array($result) && array_key_exists('items', $result)) {
            foreach ($result["items"] as $item) {
                $id = getId($item["href"]);
                $resultResource = querySRApi($this->apiEndpoint . "/" . $id, [], "GET", "responseBody", false);
//                sout($resultResource);
//                die();
                $resource = $this->create();
                $resource->setAllAttributeByArray($resultResource);

                return $resource;
            }
        } else {
            if ($counnter == 3) {
                sout("nincs sku getBySku(): " . $sku);
                return array();
            }
            $this->getBySku($sku, $counnter);
        }
    }

}
