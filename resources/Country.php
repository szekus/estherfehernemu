<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of Costumer
 *
 * @author tamas
 */
class Country extends Resource {

    public static function create() {
        $resource = new Country();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/countries";
        $this->dataColumns = IResource::COUNTRY_ARRAY;
    }

    public function getSRCountryIdByIsoCode2($isoCode2) {
        $query = "SELECT id FROM " . $this->getResourceToDB()->getTableName() . " WHERE isoCode2 = '$isoCode2' ";
        $result = $this->getResourceToDB()->getDb()->findOne($query, "id");
//        sout($query);  
        return $result;
    }
    
   

    public function createSRId($idNumber) {
        return base64_encode("country-country_id=" . $idNumber);
    }

    public function getCountryNameAndCodeFromLink($countryLink) {
//        $id = getId($countryLink);
//        $query = "SELECT * FROM aaasr_countries WHERE id = '$id'";
//        $result = $this->resourceToDB->getDB()->query($query);
//        return $result[0];

        $result = array();
        $result["name"] = "Magyarország";
        $result["isoCode2"] = "HU";
        return $result;
    }

}
