<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of CustomerGroupProductPrice
 *
 * @author tamas
 */
class CustomerGroupProductPrice extends Resource {

    public static function create() {
        $resource = new CustomerGroupProductPrice();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/customerGroupProductPrices";
        $this->dataColumns = IResource::CUSTOMER_GROUP_PRODUCT_ARRAY;
    }

}
