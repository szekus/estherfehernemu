<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

define("MAGIC_CUSTOMER_NUMBER", 10000);

/**
 * Description of Costumer
 *
 * @author tamas
 */
class Customer extends Resource {

    public static function create() {
        $resource = new Customer();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        $resource->setUp();
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/customers";
        $this->dataColumns = IResource::CUSTOMER_ARRAY;
    }

//    public function getAsArray() {
//        return array_merge(parent::getAsArray(), get_object_vars($this));
//    }

    public function createSRId($idNumber) {
        return base64_encode("customer-customer_id=" . $idNumber);
    }

    public function getPhoneNumberByCustomerId($customerId) {
        $query = "SELECT * FROM ps_address WHERE id_customer = $customerId";
        $telephone = "+36 (20) 123 4567";
        if ($this->resourceToDB->getDb()->isExist($query)) {
            $result = $this->resourceToDB->getDb()->query($query);
            $telephone = $result[0]["phone_mobile"];

            if ($telephone == "") {
                $telephone = $result[0]["phone"];
            }
        }
        return $telephone;
    }

//    public function createEMCustomerId($SRCustomerInnerId) {
//        
//        return (int) ((int) $SRCustomerInnerId + MAGIC_CUSTOMER_NUMBER);
//    }
}
