<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductCategoryRelation
 *
 * @author tamas
 */
class ProductCategoryRelation extends Resource {

    public static function create() {
        $resource = new ProductCategoryRelation();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/productCategoryRelations";
        $this->dataColumns = IResource::PRODUCT_CATEGORY_RELATION_ARRAY;
    }

}
