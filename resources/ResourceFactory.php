<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ResourceFactory
 *
 * @author tamas
 */
class ResourceFactory {

    public static function getInstance() {
        static $instance = null;
        if (null === $instance) {
            $instance = new static;
        }

        return $instance;
    }

    protected function __construct() {
        
    }

    private function __clone() {
        
    }

    public function build($shopName) {
        $resource = null;
        switch ($shopName) {
            case "l_a_v_d_r":
                $resource = ListAttributeValueDescription::create();
                break;
            case "p_l_a_v_r_r":
                $resource = ProductListAttributeValueRelation::create();
                break;
            case "l_a_v_r":
                $resource = ListAttributeValue::create();
                break;
            case "o_s":
                $resource = OrderStatus::create();
                break;
            case "p_class_a_r_r":
                $resource = ProductClassAttributeRelation::create();
                break;
            case "l_a_r":
                $resource = ListAttribute::create();
                break;
            case "a_d_r":
                $resource = AttributeDescription::create();
                break;
            case "a_r":
                $resource = Address::create();
                break;
            case "cat_r":
                $resource = Category::create();
                break;
            case "cat_desc":
                $resource = CategoryDescription::create();
                break;
            case "p_r":
                $resource = Product::create();
                break;
            case "p_d_r":
                $resource = ProductDescription::create();
                break;
            case "p_class_r":
                $resource = ProductClass::create();
                break;
            case "p_cat_r":
                $resource = ProductCategoryRelation::create();
                break;
            case "p_i_r":
                $resource = ProductImage::create();
                break;
            case "p_t_r":
                $resource = ProductTag::create();
                break;
            case "p_s_r":
                $resource = ProductSpecial::create();
                break;
            case "p_c_r_r":
                $resource = ProductCategoryRelation::create();
                break;
            case "p_o_r":
                $resource = ProductOption::create();
                break;
            case "p_o_d_r":
                $resource = ProductOptionDescription::create();
                break;
            case "p_o_v_r":
                $resource = ProductOptionValue::create();
                break;
            case "p_o_v_d_r":
                $resource = ProductOptionValueDescription::create();
                break;
            case "p_rel_p_r_r":
                $resource = ProductRelatedProductRelation::create();
                break;
            case "p_col_p_r_r":
                $resource = ProductCollateralProductRelation::create();
                break;
            case "p_p_b_r_r":
                $resource = ProductProductBadgeRelation::create();
                break;
            case "u_a_r":
                $resource = URLAlias::create();
                break;
            case "m_r":
                $resource = Manufacturer::create();
                break;
            case "cus_r":
                $resource = Customer::create();
                break;
            case "cus_group":
                $resource = CustomerGroup::create();
                break;
            case "l_p":
                $resource = LoyaltyPoint::create();
                break;

            default: throw new Exception("Resource doesn't exist!");
        }

        return $resource;
    }

}
