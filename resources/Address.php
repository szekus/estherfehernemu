<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of Address
 *
 * @author tamas
 */
class Address extends Resource {


    public static function create() {
        $resource = new Address();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/addresses";
        $this->dataColumns = IResource::ADDRESS_ARRAY;
    }

   

}
