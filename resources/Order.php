<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

define("MAGIC_ORDER_NUMBER", 3000);
define("MAGIC_LINE_NUMBER", 100000);

/**
 * Description of Order
 *
 * @author tamas
 */
class Order extends Resource implements IResource {

    public static function create() {
        $resource = new Order();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    protected function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/orders";
        $this->dataColumns = IResource::ORDER_ARRAY;
    }

    public function createEMOrderId($SROrderInnerId) {
        return (int) ((int) $SROrderInnerId + MAGIC_ORDER_NUMBER);
    }

    public function setTermDeliv($SROrder) {
        $reult = 0;
        switch ($SROrder["shippingMethodName"]) {
            case "Házhozszállítás futárszolgálattal":
                $result = 0;
                break;
            case "Személyes átvétel":
                $result = 1;
                break;
            case "Pickup pont":
                $result = 2;
                break;
            default : $result = 1;
                break;
        }

        return $result;
        ;
    }

    public function setTermPay($SROrder) {
        $reult = 0;
        switch ($SROrder["paymentMethodCode"]) {
            //Utánvétel
            case "COD":
                $result = 85;
                break;
            //Készpénzes fizetés
            case "COD2":
                $result = 85;
                break;
            //Banki átutalás (előre utalás)
            case "BANK_TRANSFER":
                $result = 0;
                break;
            //OTP Bankkártyás fizetés
            case "OTP":
                $result = 86;
                break;
            default : $result = 0;
                break;
        }

        return $result;
        ;
    }

    public function setCurrCode($SROrder) {
        $id = getId($SROrder["currency"]);
        $query = "SELECT * FROM aaasr_currencies WHERE id = '$id'";
        $result = $this->getResourceToDB()->getDb()->query($query);
        switch ($result[0]["name"]) {
            case "HUF":
                return "HUF";
//                return "00";
            case "EUR":
                return "EUR";
//                return "06";
            default :
                return "HUF";
//                return "00";
        }
    }

    public function setLineNum($SROrderProduct) {
        $SRId = $SROrderProduct["id"];
        $SRId = base64_decode($SRId);
        $id = str_replace("orderProduct-order_product_id=", "", $SRId);

        return (int) ((int) $id + MAGIC_LINE_NUMBER);
    }

    public function setLineNumSZALL($SROrderProduct) {
//        $SRId = $SROrderProduct["id"];
//        $SRId = base64_decode($SRId);
//        $id = str_replace("orderProduct-order_product_id=", "", $SRId);
//
//        return (int) ((int) $id + MAGIC_LINE_NUMBER);

        return "099999";
    }

    public function hasComment($SROrder) {
        if ($SROrder["comment"] == "") {
            return false;
        }
        return true;
    }

    public function getSZALLUnitPrice($SROrder) {
        $totalPrice = $SROrder["total"];
        $orderId = $SROrder["id"];
        $orderSRLink = SR_APIURL . "/orders/$orderId";
        $query = "SELECT * FROM aaasr_orderProducts AS op WHERE op.order like '$orderSRLink'";
        $orderProducts = $this->resourceToDB->getDb()->query($query);
        $sumGross = 0;
        foreach ($orderProducts as $orderProduct) {
            $grossPrice = getGrossPrice($orderProduct["total"], $orderProduct["taxRate"]);
            $sumGross += $grossPrice;
        }


        $SZALLPrice = round($totalPrice - $sumGross);

        return getNetPrice(abs($SZALLPrice), 27);
    }

    public function getOrderProductsId($SROrder) {
        $idWithAttr = getId($SROrder["orderProducts"]);

        return str_replace("orderProducts?orderId=", "", $idWithAttr);
    }

    public function getOrderLink($SROrder) {
        return SR_APIURL . "/orders/" . $SROrder["id"];
    }

    public function createBGOrders($list) {
        $shipping = array();
        $line_items = array();

        $result = array();

        foreach ($list as $item) {

            $order = array();

            $order["innerId"] = $item["innerId"];

            $shipping["first_name"] = $item["shippingFirstname"];
            $shipping["last_name"] = $item["shippingLastname"];

            $shipping["first_name"] = "TEST";
            $shipping["last_name"] = "TEST";

            if ($item["shippingZoneName"] == "") {
                $shipping["state"] = "CA";
            } else {
                $shipping["state"] = $item["shippingZoneName"];
            }

//            $shipping["state"] = $item["shippingZoneName"];
            $shipping["address_1"] = $item["shippingAddress1"];
            $shipping["address_2"] = $item["shippingAddress2"];
            $shipping["city"] = $item["shippingCity"];

            $shipping["postcode"] = $item["shippingPostcode"];
            $shipping["country"] = $item["shippingCountryName"];
            $order["shipping"] = $shipping;
//            var_dump($item["orderProducts"]["href"]);;
            $hrefs = $this->getProductsLinkFromOrderProductsLink($item["orderProducts"]["href"]);
            $products = $this->getProductsFromOrderProductsLink($hrefs);
            foreach ($products as $prod) {
                $l_item = array();
                $l_item["quantity"] = $prod["quantity"];
                $bgProduct = new \bgresource\BGProduct();
                $bgProduct->getBySku($prod["parent_sku"]);

                if ($prod["parent_sku"] != $prod["child_sku"]) {
                    $arr = explode("-", $prod["child_sku"]);
                    $increment = $arr[count($arr) - 1] - 1;
                    $variations = $bgProduct->getVariations();
                    if (isset($variations[$increment])) {
                        $l_item["product_id"] = $bgProduct->getId();
                        $l_item["variation_id"] = ($variations[$increment]->getId());
                    } else {
                        echo "NO_PRODUCT_ID => ";
                        echo 'sku: ' . $prod["child_sku"] . "\n";
                    }
                } else {
                    $l_item["variation_id"] = $bgProduct->getId();
                }
                $line_items[] = $l_item;
            }
            $order["line_items"] = $line_items;
            $result[] = $order;
//            break;
        }
        return $result;
    }

    public function getProductsLinkFromOrderProductsLink($link) {
        $hrefs = array();
        $id = getParamValue($link, "orderId");
        $url = "/orderProducts?orderId=" . $id;
        $result = querySRApi($url, [], "GET");
//        var_dump($url);
        foreach ($result["items"] as $item) {
            $hrefs[] = $item["href"];
        }
        return $hrefs;
    }

    function getProductsFromOrderProductsLink($hrefs) {
        $products = array();
        foreach ($hrefs as $href) {
            $id = getId($href);
//            echo "/orderProducts/" . $id;
            $result = querySRApi("/orderProducts/" . $id, [], "GET");
//            $products["sku"] = $result["sku"];
            $productHref = $result["product"]["href"];
            $id = getId($productHref);

            $res = querySRApi("/products/" . $id, [], "GET");
            if (!key_exists("error", $res)) {
                $quantity = $result["stock1"];


                for ($i = 2; $i <= 4; $i++) {
                    $stock = "stock" . $i;
                    $quantity += $result[$stock];
                }

//            var_dump($quantity);

                $parentProductHref = $res["parentProduct"]["href"];

                $parentId = getId($parentProductHref);

                $product = array();
                $product["quantity"] = $quantity;
                $product["child_sku"] = $res["sku"];

                if ($id == $parentId) {
                    $product["parent_sku"] = $res["sku"];
                } else {
                    $parentProduct = querySRApi("/products/" . $parentId, [], "GET");
                    $product["parent_sku"] = $parentProduct["sku"];
                }

//                var_dump($parentId);
                $product["child_sku"] = "SIG19069-3";
                $product["parent_sku"] = "SIG19069";

                $products[] = $product;
            }
        }

        return $products;
    }

    public function switchStatus($param) {
        $status = ORDER_STATUS_0;
        switch ($param) {
            case "0":
                $status = ORDER_STATUS_0;
                break;
            case "1":
                $status = ORDER_STATUS_1;
                break;
            case "2":
                $status = ORDER_STATUS_2;
                break;
            case "3":
                $status = ORDER_STATUS_3;
                break;
            case "4":
                $status = ORDER_STATUS_4;
                break;

            default:
                $status = ORDER_STATUS_0;
                break;
        }

        return $status;
    }

    public function createIdFromEMId($em_id) {
        $em_id = (int) $em_id;
        return (int) ($em_id - MAGIC_ORDER_NUMBER);
    }

    public function createSRIdFromEmId($em_id) {
        $id = $this->createIdFromEMId($em_id);
        return base64_encode("order-order_id=$id");
    }

}
