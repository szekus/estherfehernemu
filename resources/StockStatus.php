<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of StockStatus
 *
 * @author tamas
 */
class StockStatus extends Resource {

    public static function create() {
        $resource = new StockStatus();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/stockStatuses";
        $this->dataColumns = IResource::STOCK_STATUS_ARRAY;
    }

    public function switchId($id) {
        switch ($id) {
            case "5": return STOCK_STATUS_5;
            case "8": return STOCK_STATUS_8;
            case "9": return STOCK_STATUS_9;
            case "10": return STOCK_STATUS_10;
            case "11": return STOCK_STATUS_11;
        }
        return SR_STATUS_RAKTARON;
    }

}
