<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of URLAlias
 *
 * @author tamas
 */
class URLAlias extends Resource {

    public static function create() {
        $resource = new URLAlias();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/urlAliases";
        $this->dataColumns = IResource::URLALIAS_ARRAY;
    }

    
    private function deleteUrl($type) {
        $endpointPath = $this->apiEndpoint . "?type=" . $type . "&page=0&limit=200";

        do {
            $result = querySRApi($endpointPath, [], 'GET');
            $batchRequest['requests'] = [];
            foreach ($result['items'] as $item) {
                // print_r($item);
                $batchRequest['requests'][] = [
                    'method' => 'DELETE',
                    // 'method' => 'GET',
                    'uri' => $item['href']
                ];
                //  data dump - var_dump(querySRApi(str_replace(SR_APIURL,'', $item['href']), [], 'GET'));
            }
            // TORLES
            if (is_array($batchRequest) && count($batchRequest['requests']) > 0) {
                print_r(querySRApi(API_ENDPOINT_BATCH, $batchRequest, 'POST'));
            }
        } while (!is_null($result['next']));
    }

    public function deleteProductUrl() {
        $this->deleteUrl("PRODUCT");
    }

    public function deleteCategoryUrl() {
        $this->deleteUrl("CATEGORY");
    }

}
