<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of LoyaltyPointUsed
 *
 * @author szekus
 */
class LoyaltyPointUsed extends Resource {

    public static function create() {
        $resource = new LoyaltyPointUsed();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/loyaltyPointsUsed";
        $this->dataColumns = IResource::LOYALTY_POINT_USED_ARRAY;
    }

}
