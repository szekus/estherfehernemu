<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductOptionValue
 *
 * @author szekus
 */
class ProductOptionValue extends Resource {

    public static function create() {
        $resource = new ProductOptionValue();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/productOptionValues";
        $this->dataColumns = IResource::PRODUCT_OPTION_VALUE_ARRAY;
    }

}
