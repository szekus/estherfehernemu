<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductCollateralProductRelation
 *
 * @author szekus
 */
class ProductCollateralProductRelation extends Resource {

    public static function create() {
        $resource = new ProductCollateralProductRelation();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/productCollateralProductRelations";
        $this->dataColumns = IResource::PRODUCT_COLLATERAL_PRODUCT_RELATION_ARRAY;
    }

}
