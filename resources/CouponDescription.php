<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of CouponDescription
 *
 * @author szekus
 */
class CouponDescription extends Resource {

    public static function create() {
        $resource = new CouponDescription();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    protected function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/couponDescriptions";
        $this->dataColumns = IResource::COUPON_DESCRIPTION_ARRAY;
    }

}
