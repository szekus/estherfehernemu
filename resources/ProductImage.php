<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductImage
 *
 * @author szekus
 */
class ProductImage extends Resource {

    

    public static function create() {
        $resource = new ProductImage();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/productImages";
        $this->dataColumns = IResource::PRODUCT_IMAGES_ARRAY;
    }


}
