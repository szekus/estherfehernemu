<?php

namespace resources;

class OrderProductOption extends Resource implements IResource {

    public static function create() {
        $resource = new OrderProductOption();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    protected function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/orderProductOptions";
        $this->dataColumns = IResource::ORDER_PRODUCT_OPTION_ARRAY;
    }

}
