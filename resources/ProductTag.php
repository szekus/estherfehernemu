<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductTag
 *
 * @author szekus
 */
class ProductTag extends Resource {

    public static function create() {
        $resource = new ProductTag();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        $resource->setUp();
        return $resource;
    }

    protected function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/productTags";
        $this->dataColumns = IResource::PRODUCT_TAG_ARRAY;
    }

}
