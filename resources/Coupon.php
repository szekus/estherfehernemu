<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of Coupon
 *
 * @author szekus
 */
class Coupon extends Resource {

    public static function create() {
        $resource = new Coupon();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    protected function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/coupons";
        $this->dataColumns = IResource::COUPON_ARRAY;
    }

    public function createSRId($couponId) {
        return base64_encode("coupon-coupon_id=" . $couponId);
    }

}
