<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of CustomerGroup
 *
 * @author szekus
 */
class CustomerGroup extends Resource {

    public $groups;

    public static function create() {
        $resource = new CustomerGroup();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/customerGroups";
        $this->dataColumns = IResource::CUSTOMER_GROUP_ARRAY;
    }

    public function createSRId($idNumber) {
        return base64_encode("customerGroup-customer_group_id=" . $idNumber);
    }

    public function createOuterId($groupId) {
        return APP_NAME . "_customer_group_$groupId";
    }

    public function setCustomerGroups() {
        $db = \db\Database::instance();
        $query = "SELECT * FROM aaasr_customergroups";
        $result = $db->query($query);


        foreach ($result as $value) {
            $group = array();
            $name = $value["name"];
            $query = "SELECT * FROM bioterme_customer_group WHERE name = '$name'";
            $res = $db->query($query);
            $oneGroup = $res[0];
            $group["name"] = $name;
            $group["customer_group_id"] = $oneGroup["customer_group_id"];
            $group["SRID"] = $value["id"];
            $this->groups[] = $group;
        }
    }

    public function switchId($id) {
        $cusmoterGroupOuterId = "";
        $db = \db\Database::instance();
        $query = "SELECT * FROM gomb_user_usergroup_map WHERE user_id = $id ORDER BY group_id DESC";
        $result = $db->query($query);
        if (count($result) > 0) {
            $cusmoterGroupOuterId = "customer_group_" . $result[0]["group_id"];
        } else {
            $cusmoterGroupOuterId = "customer_group_2";
        }

        return $cusmoterGroupOuterId;
    }

    public function deleteAll() {
        $endpointPath = $this->apiEndpoint . "?page=0&limit=200";



        do {
            $result = querySRApi($endpointPath, [], 'GET');
            $batchRequest['requests'] = [];
            foreach ($result['items'] as $item) {
                $id = getId($item['href']);

                if (strpos($id, APP_NAME . '_') !== false) {
                    $batchRequest['requests'][] = [
                        'method' => 'DELETE',
                        'uri' => $item['href']
                    ];
                }

                // user data dump - var_dump(querySRApi(str_replace(SR_APIURL,'', $item['href']), [], 'GET'));
            }
//            sout(count($batchRequest['requests']));
            // TORLES
            if (is_array($batchRequest) && count($batchRequest['requests']) > 0) {
                print_r(querySRApi(API_ENDPOINT_BATCH, $batchRequest, 'POST', true));
            }
        } while (!is_null($result['next']));

        echo "DELETED $this->apiEndpoint\n\n";
    }

}
