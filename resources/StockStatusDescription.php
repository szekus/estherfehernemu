<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of StockStatusDescription
 *
 * @author tamas
 */
class StockStatusDescription extends Resource {

    public static function create() {
        $resource = new StockStatusDescription();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/stockStatusDescriptions";
        $this->dataColumns = IResource::STOCK_STATUS_DESCRIPTION_ARRAY;
    }

}
