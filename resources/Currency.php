<?php

namespace resources;

class Currency extends Resource {

    public static function create() {
        $resource = new Currency();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/currencies";
        $this->dataColumns = IResource::CURRENCY_ARRAY;
    }

}
