<?php

namespace resources;

class LengthClass extends Resource {

    public static function create() {
        $resource = new LengthClass();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/lengthClasses";
        $this->dataColumns = IResource::LENGTH_CLASS_ARRAY;
    }

    public function switchId($id) {
        switch ($id) {
            case "1": return LENGTH_CLASS_CM;
            case "2":return LENGTH_CLASS_DM;
        }
        return LENGTH_CLASS_CM;
    }

}
