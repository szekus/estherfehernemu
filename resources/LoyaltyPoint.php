<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of LoyaltyPoint
 *
 * @author szekus
 */
class LoyaltyPoint extends Resource {

    public static function create() {
        $resource = new LoyaltyPoint();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/loyaltyPoints";
        $this->dataColumns = IResource::LOYALTY_POINT_ARRAY;
    }

}
