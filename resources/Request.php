<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of Request
 *
 * @author szekus
 */
class Request {

    private $request;

    function getRequest() {
        return $this->request;
    }

    function setRequest($request) {
        $this->request = $request;
    }

    public static function create() {
        return new Request();
    }

    private function __construct() {
        ;
    }

    public function addBatch($array) {
        $this->request["requests"][] = $array;
    }

    public function run($debug = false) {

        $limitedRequest["requests"] = array();
        $allRequest = array();
        $resultArray = array();

        if ($this->request != null) {
            $i = 0;
            foreach ($this->request["requests"] as $value) {
                $limitedRequest["requests"][] = $value;
                if ($i == 179) {
                    $allRequest[] = $limitedRequest;
                    $limitedRequest = array();
                    $i = -1;
                }
                $i++;
            }
            if (count($limitedRequest) > 0) {
                $allRequest[] = $limitedRequest;
            }
            
            foreach ($allRequest as $value) {
                $result = querySRApi("/batch", $value, "POST", "batch", $debug);
                
                if ($debug) {
                    sout($result);
                }

                $resultArray[] = $result;
            }
        }


        return $resultArray;
    }

}
