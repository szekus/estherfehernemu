<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductListAttributeValueRelation
 *
 * @author szekus
 */
class ProductListAttributeValueRelation extends Resource {

    public static function create() {
        $resource = new ProductListAttributeValueRelation();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/productListAttributeValueRelations";
        $this->dataColumns = IResource::LIST_ATTRIBUTE_VALUE_RELATION_ARRAY;
    }

}
