<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of WeightClass
 *
 * @author tamas
 */
class WeightClass extends Resource {

    public static function create() {
        $resource = new WeightClass();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/weightClasses";
        $this->dataColumns = IResource::WEIGHT_CLASS_ARRAY;
    }

    public function switchId($id) {
        switch ($id) {
            case "g":
            case "gr":
            case "mg":
                return WEIGHT_CLASS_G;
            case "KG": return WEIGHT_CLASS_KG;
        }
        return WEIGHT_CLASS_KG;
    }

}
