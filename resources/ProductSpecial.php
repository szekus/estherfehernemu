<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of ProductSpecial
 *
 * @author tamas
 */
class ProductSpecial extends Resource {

    public static function create() {
        $resource = new ProductSpecial();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/productSpecials";
        $this->dataColumns = IResource::PRODUCT_SPECIAL_ARRAY;
    }


}
