<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace resources;

/**
 * Description of TaxClass
 *
 * @author tamas
 */
class TaxClass extends Resource {

    public static function create() {
        $resource = new TaxClass();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    public function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/taxClasses";
        $this->dataColumns = IResource::TAX_CLASS_ARRAY;
    }

    public function switchId($id) {
        switch ($id) {
            case "9":
                return TAX_CLASS_27;
            case "11":
                return TAX_CLASS_18;
            case "12":
                return TAX_CLASS_05;
        }
        
        return TAX_CLASS_27;
    }

}
