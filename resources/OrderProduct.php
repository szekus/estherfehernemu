<?php

namespace resources;

class OrderProduct extends Resource implements IResource {

    public static function create() {
        $resource = new OrderProduct();
        $resource->resourceToDB = \db\ResourceToDB::create($resource);
        return $resource;
    }

    protected function __construct() {
        parent::__construct();
        $this->apiEndpoint = "/orderProducts";
        $this->dataColumns = IResource::ORDER_PRODUCT_ARRAY;
    }

}
