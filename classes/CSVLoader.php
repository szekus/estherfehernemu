<?php

namespace classes;

use classes\CSVCleaner;

class CSVLoader {

    private $fileAndPath;
    private $rows;
    private $rowData;
    private $rowHeader;
    private $noHeader;
    private $length;
    private $delimiter;
    private $enclosure;
    private $escape;
    protected $rowCleanerFunctionsReg;
    protected $rowCleanerFunctions;
    protected $rowCleanerFunctionsArg;

    function __construct($fileAndPath) {
        $this->fileAndPath = $fileAndPath;
        $this->noHeader = false;
        $this->length = 100;
        $this->delimiter = ',';
        $this->enclosure = '"';
        $this->escape = "\\";
        $this->rowCleanerFunctions = [];
    }

    /* Setters */

    public function setNoHeader($val) {
        $this->noHeader = $val;
    }

    // Should bigger than the longest line length
    public function setLength($val) {
        $this->length = $val;
    }

    public function setDelimiter($val) {
        $this->delimiter = $val;
    }

    public function setEnclosure($val) {
        $this->enclosure = $val;
    }

    public function setEscape($val) {
        $this->escaape = $val;
    }

    public function registerRowCleanerFunction($CSVLeanerMethod, $args, $priority = 10) {
        if (!isset($this->rowCleanerFunctionsReg[$CSVLeanerMethod])) {
            $this->rowCleanerFunctions[$priority] = $CSVLeanerMethod;
            $this->rowCleanerFunctionsReg[$CSVLeanerMethod] = $priority;
            $this->rowCleanerFunctionsArg[$CSVLeanerMethod] = $args;
            sort($this->rowCleanerFunctions);
        }
    }

    protected function runRowCleaners($row) {

        if (!count($this->rowCleanerFunctions)) {
            return $row;
        }

        $rows = ['0' => $row];

        foreach ($this->rowCleanerFunctions as $methodName) {
            $cleaner = new CSVCleaner($rows);
            if (method_exists($cleaner, $methodName)) {
                $cleaner->{$methodName}($this->rowCleanerFunctionsArg[$methodName]);
                $rows = $cleaner->getCleaned();
            }
        }
        return array_shift($rows);
    }

    /* CSV processing */

    public function loadCSV() {
        $counter = 0;
        if (($handler = fopen($this->fileAndPath, "r")) !== FALSE) {

            while (($row = fgetcsv($handler, $this->length, $this->delimiter, $this->enclosure, $this->escape)) !== FALSE) {
                //  $this->rows[] = $row;

                $newRow = $this->runRowCleaners($row);
//              var_dump($newRow);
                //    $counter++;
                //  if ($counter>3)
                //  exit;
                if (is_array($newRow) && count($newRow) > 0) {
                    $this->rows[] = $newRow;
                }
            }
            fclose($handler);
        }
    }

    public function parseHeader() {
        $this->rowHeader = $this->rows[0];
    }

    public function parseData() {
        $copyIndex = 1;
        if ($this->noHeader) {
            $copyIndex = 0;
        }
        $this->rowData = array_slice($this->rows, $copyIndex);
        $cleaner = new CSVCleaner($this->rowData);
        $cleaner->removeEmptyLines();
        $this->rowData = $cleaner->getCleaned();
    }

    public function getHeader() {
        return $this->rowHeader;
    }

    public function getData() {
        return $this->rowData;
    }

    public function export($fname) {
        $fp = fopen($fname, 'w');
        fputcsv($fp, $this->rowHeader);
        foreach ($this->rowData as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
    }

    public function debug($skipRaw = true) {
        echo 'Header:';
        print_r($this->rowHeader);

        echo 'Data:';
        print_r($this->rowData);
        if (!$skipRaw) {
            echo 'Raw Rows:';
            print_r($this->rows);
        }
    }

}
