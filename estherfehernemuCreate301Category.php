<?php

echo '<pre>';

require_once('loader.php');

$db = \db\Database::instance();


$categories = $db->query(CATEGORY_QUERY);

foreach ($categories as $category) {

    $categoryId = $category[CATEGORY_ID];
    $query = "SELECT * FROM uj_category_path WHERE category_id = $categoryId ORDER BY level ASC";
    if ($db->isExist($query)) {
        $resultPath = $db->query($query);

        $oldUrl = "";
        foreach ($resultPath as $path) {
            $pathId = $path["path_id"];
            $query = "SELECT * FROM uj_url_alias WHERE query = 'category_id=$pathId'";
            $aliasArray = $db->query($query);
            $keyword = $aliasArray[0]["keyword"];
            $oldUrl .= "$keyword/";
        }
    }

    $newUrl = slug($category["name"]) . "-" . $categoryId;

    $categoryUrls[] = array(
        "oldUrl" => $oldUrl,
        "newUrl" => $newUrl,
        "order" => 0
    );
}


$fp = fopen('data/' . APP_NAME . 'CategoryUrl.csv', 'w');

$headers = ['old_url', 'new_url', 'order'];
fputcsv($fp, $headers, ";");
foreach ($categoryUrls as $fields) {
    fputcsv($fp, array_values($fields), ";");
}
fclose($fp);

$oldURLlink = SHOP_URL;
$newURLlink = SR_SHOP_LINK;

foreach ($categoryUrls as $categoryUrl) {
    echo "<a href='" . $oldURLlink . $categoryUrl["oldUrl"] . "' target='_blank'>" . $oldURLlink . $categoryUrl["oldUrl"] . "</a> || "
    . "<a href='" . $newURLlink . $categoryUrl["newUrl"] . "' target='_blank'>" . $newURLlink . $categoryUrl["newUrl"] . "</a> || ";
    echo '<br>';
}
