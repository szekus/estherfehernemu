<?php

echo '<pre>';

require_once('loader.php');

$db = \db\Database::instance();




$products = $db->query(PRODUCT_QUERY);


$productUrls = array();
foreach ($products as $product) {
    $productId = $product[PRODUCT_ID];


    $newUrl = slug($product["name"] . "-" . $productId);

    $query = "SELECT * FROM  uj_url_alias WHERE query = 'product_id=$productId'";
    if ($db->isExist($query)) {
        $resultAliasArray = $db->query($query);
        $oldUrl = $resultAliasArray[0]["keyword"];
        $productUrls[] = array(
            "oldUrl" => $oldUrl,
            "newUrl" => $newUrl,
            "order" => 0
        );
    }
}


$fp = fopen('data/' . APP_NAME . 'ProductUrl.csv', 'w');

$headers = ['old_url', 'new_url', 'order'];
fputcsv($fp, $headers, ";");
foreach ($productUrls as $fields) {
    fputcsv($fp, array_values($fields), ";");
}
fclose($fp);

$oldURLlink = SHOP_URL;
$newURLlink = SR_SHOP_LINK;

foreach ($productUrls as $productUrl) {
    echo "<a href='" . $oldURLlink . $productUrl["oldUrl"] . "' target='_blank'>" . $oldURLlink . $productUrl["oldUrl"] . "</a> || "
    . "<a href='" . $newURLlink . $productUrl["newUrl"] . "' target='_blank'>" . $newURLlink . $productUrl["newUrl"] . "</a> || ";
    echo '<br>';
}


