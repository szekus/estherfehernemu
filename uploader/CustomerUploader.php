<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace uploader;

/**
 * Description of CustomerUploader
 *
 * @author tamas
 */
class CustomerUploader extends Uploader {

    protected function __construct() {
        parent::__construct();
    }

    public static function create() {
        $uploader = new CustomerUploader();
        return $uploader;
    }

    public function run() {

        $customerHelper = \helpers\CustomerHelper::create();

        $customerHelper->setCustomers(false);


        $list = array(
//            "cus_group",
            "cus_r",
            "a_r",
//            "l_p",
        );
        $customerHelper->deleteCustomer($list);


//        $customerHelper->insertCustomerGroup();

        $customerHelper->insertCustomer();
        $customerHelper->insertCustomerAddress();

//        $customerHelper->insertLoyalPoint();
    }

}
