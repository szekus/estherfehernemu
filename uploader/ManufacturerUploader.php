<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace uploader;

/**
 * Description of ManufacturerUploader
 *
 * @author tamas
 */
class ManufacturerUploader extends Uploader{

    protected function __construct() {
        parent::__construct();
    }

    public static function create() {
        $uploader = new ManufacturerUploader();
        return $uploader;
    }

    public function run() {
        $manufacturerHelper = \helpers\ManufacturerHelper::create();

        $manufacturerHelper->deleteManufacturer();
      
        $manufacturerHelper->setManufacturers();
        
        $manufacturerHelper->insertManufacturer();
          
//        $manufacturerHelper->insertManufacturerDescription();
        
    }

}
