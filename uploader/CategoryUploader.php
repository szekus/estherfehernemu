<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace uploader;

/**
 * Description of CategoryUploader
 *
 * @author tamas
 */
class CategoryUploader extends Uploader {

    protected function __construct() {
        parent::__construct();
    }

    public static function create() {
        $uploader = new CategoryUploader();
        return $uploader;
    }

    public function run() {

        $categoryHelper = \helpers\CategoryHelper::create();
        
        $categoryHelper->setCategories(false);

        $list = array(
            "cat_r",
            "cat_desc",
            "u_a_r_cat"
        );
        $categoryHelper->deleteCategory($list);


        sout("insertCategory");
        $categoryHelper->insertCategory();
        
      
        sout("insertCategoryDescription");
        $categoryHelper->insertCategoryDescription();

        sout("updateCategoryParent");
        $categoryHelper->updateCategoryParent();

        sout("insertCategoryUrlAlias");
        $categoryHelper->insertCategoryUrlAlias();
    }

}
