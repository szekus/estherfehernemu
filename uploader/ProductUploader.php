<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace uploader;

/**
 * Description of ProductUploader
 *
 * @author tamas
 */
class ProductUploader extends Uploader {

    protected function __construct() {
        parent::__construct();
    }

    public static function create() {
        $uploader = new ProductUploader();
        return $uploader;
    }

    public function run() {

        $productHelper = \helpers\ProductHelper::create();

        $productHelper->setProducts(true);

        $list = array(
            "p_d_r",
            "p_cat_r",
            "u_a_r_prod",
            "p_i_r",
            "p_p_b_r_r",
            "p_s_r",
            "p_rel_p_r_r",
//////            "p_class_a_r_r",
//////            "p_class_r",
//////            "l_a_r",
            "p_r",
        );

        $productHelper->insertListAttributeValue(false, "DELETE");
        $productHelper->deleteProduct($list);

        sout("insertProduct");
        $productHelper->insertProduct(true);

        sout("insertRelatedProductRelations");
        $productHelper->insertRelatedProductRelations(true);


        sout("insertProductDescription");
        $productHelper->insertProductDescription(true);

        sout("insertProductCategoryRelation");
        $productHelper->insertProductCategoryRelation(true);


        sout("insertProductUrlAlias");
        $productHelper->insertProductUrlAlias(true);

        sout("insertProductImage");
        $productHelper->insertProductImage(true);


        sout("insertProductSpecial");
        $productHelper->insertProductSpecial(true);


//////        sout("insertListAttribute");
//////        $productHelper->insertListAttribute(true);
//////        sout("insertAttributeDescription");
//////        $productHelper->insertAttributeDescription(true);
//////        sout("insertProductClassAttributeRelation");
//////        $productHelper->insertProductClassAttributeRelation(true);

        sout("insertListAttributeValue");
        $productHelper->insertListAttributeValue(true, "POST");



//        $productHelper->insertProductTag(true);
//        
//        $productHelper->insertProductParent(true);
//        $productHelper->insertProductClass(true);
//        $productHelper->updateProductClass(true);
//        
//        $productHelper->insertProductOption(true);
//        $productHelper->updateProductProductClass(true);
//       
//        
//        
    }

}
