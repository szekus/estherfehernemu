<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace uploader;

/**
 * Description of CouponUploader
 *
 * @author szekus
 */
class CouponUploader extends Uploader {

    protected function __construct() {
        parent::__construct();
    }

    public static function create() {
        $uploader = new CouponUploader();
        return $uploader;
    }

    public function run() {

        $couponHelper = \helpers\CouponHelper::create();

        $couponHelper->setCoupons(false);

        $list = array(
            "coupon",
            "coupon_description",
//            "coupon_product_relation",
            "coupon_category_relation"
        );
        $couponHelper->deleteCoupon($list);


        sout("insertCoupon");
        $couponHelper->insertCoupon(true);

        sout("insertCouponDescription");
        $couponHelper->insertCouponDescription(true);

//        sout("insertCouponProductRelation");
//        $couponHelper->insertCouponProductRelation(true);
        
        sout("insertCouponCategoryRelation");
        $couponHelper->insertCouponCategoryRelation(true);
    }

}
