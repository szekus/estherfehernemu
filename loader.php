<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//$cwd = getcwd();

//require_once($cwd.'/classes/CSVLoader.php');
//require_once($cwd.'/classes/CSVCleaner.php');
require_once('classes/ApiCall.php');
require_once('helpers/shoprenter.php');
require_once('helpers/utils.php');
require_once('settings.php');
require_once('query.php');

spl_autoload_register(function ($class_name) {
    $class_name = str_replace('\\', '/', $class_name);
    include $class_name . '.php';
});
