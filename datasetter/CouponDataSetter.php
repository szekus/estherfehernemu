<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace datasetter;

/**
 * Description of CouponDataSetter
 *
 * @author szekus
 */
class CouponDataSetter extends DataSetter {

    protected function __construct() {
        parent::__construct();
    }

    public static function create() {
        $couponDataSetter = new CouponDataSetter();
        return $couponDataSetter;
    }

    public function setData() {
        $this->data = $this->db->query(COUPON_QUERY);

        foreach ($this->data as &$coupon) {
            $coupon["percentDiscountValue"] = "";
            $coupon["fixDiscountValue"] = "";

            if ($coupon["type"] == "P") {
                $coupon["discountType"] = "PERCENT";
                $coupon["percentDiscountValue"] = $coupon["discount"];
            } else {
                $coupon["discountType"] = "FIXED";
                $coupon["fixDiscountValue"] = $coupon["discount"];
            }
        }
    }

}
