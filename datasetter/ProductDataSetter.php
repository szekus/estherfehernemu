<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace datasetter;

/**
 * Description of ProductDataSetter
 *
 * @author szekus
 */
class ProductDataSetter extends DataSetter {

    protected function __construct() {
        parent::__construct();
    }

    public static function create() {
        $productDataSetter = new ProductDataSetter();
        return $productDataSetter;
    }

    public function setData() {
        $this->data = $this->db->query(PRODUCT_QUERY);
        $this->updateSku();
        die();
        $this->data = $this->db->query(PRODUCT_QUERY);

        $stockStatusResource = \resources\StockStatus::create();
        $taxClassResource = \resources\TaxClass::create();

        foreach ($this->data as &$product) {
            $productId = $product[PRODUCT_ID];
//            $productParentId = $product[PRODUCT_PARENT_ID];
            $product["taxClass"] = TAX_CLASS_27;
//            $product["description"] = "";
            $product["categories"] = array();
            $product["mainPicture"] = "";
            $product["imageAlt"] = "";
            $product["images"] = array();
            $product["tag"] = "";
//            $product["price"] = 0;
            $product["options"] = array();
            $product["special"] = array();
            $product["relatedProducts"] = array();
            $product["listAttributes"] = array();
            $product["productClass"] = "";
            $product["isParent"] = false;
            $product["isChild"] = false;

            $product["loyaltyPoints"] = 0;

            $stockStatusId = $product["stock_status_id"];
            $product["inStockStatus"] = $stockStatusResource->switchId($stockStatusId);

            $product["mainPicture"] = $this->setImagesPath($product["image"]);

            $taxClassId = $product["tax_class_id"];
            $product["taxClass"] = $taxClassResource->switchId($taxClassId);

            //--images
            $images = array();
            $query = "SELECT * FROM uj_product_image WHERE product_id = $productId";
            if ($this->db->isExist($query)) {
                $productImagesArray = $this->db->query($query);

                foreach ($productImagesArray as $productImage) {
                    $img = array();
                    $img["imagePath"] = $this->setImagesPath($productImage["image"]);
                    $images[] = $img;
                }
            }
            $product["images"] = $images;

            //--realtedProducts
            $query = "SELECT * FROM uj_product_related WHERE product_id = $productId";
            if ($this->db->isExist($query)) {
                $relatedProductArray = $this->db->query($query);
                foreach ($relatedProductArray as $relatedProduct) {
                    $product["relatedProducts"][] = $relatedProduct["related_id"];
                }
            }

            //--loyalPoints
            $query = "SELECT * FROM uj_product_reward WHERE product_id = $productId";
            if ($this->db->isExist($query)) {
                $productRewardArray = $this->db->query($query);
                $product["loyaltyPoints"] = $productRewardArray[0]["points"];
            }

            //--special
            $query = "SELECT * FROM uj_product_special WHERE product_id = $productId";
            if ($this->db->isExist($query)) {
                $product["special"] = $this->db->query($query);
            }

            //--categories
            $query = "SELECT * FROM uj_product_to_category WHERE product_id = $productId";
            if ($this->db->isExist($query)) {
                $productCategoriesArray = $this->db->query($query);
                foreach ($productCategoriesArray as $productCategory) {
                    $product["categories"][] = $productCategory["category_id"];
                }
            }

            //--attributes
            $listAttributes = array();
            $query = "SELECT * FROM uj_product_attribute WHERE product_id = $productId";
            if ($this->db->isExist($query)) {
                $product["productClass"] = SR_DEFAULT_PRODUCT_CLASS;
                $productAttributeArray = $this->db->query($query);
                foreach ($productAttributeArray as $productAttribute) {
                    $listAttribute = array();
                    $listAttribute["sr_id"] = "listAttribute_" . $productAttribute[LIST_ATTRIBUTE_ID];
                    $listAttribute["value"] = $productAttribute["text"];
                    $listAttributes[] = $listAttribute;
                }
            }
            $product["listAttributes"] = $listAttributes;
        }
    }

    public function updateSku() {
        while (1) {
            $query = "SELECT model, COUNT(*) AS c FROM oc_product GROUP BY model HAVING c > 1";
            $this->data = $this->db->query(PRODUCT_QUERY);
            
            if (!$this->db->isExist($query)) {
                break;
            }
            $duplicateProducts = $this->db->query($query);

            $existSkuArray = array();
            foreach ($duplicateProducts as $duplicateProduct) {
                $arr = array();
                $arr["sku"] = $duplicateProduct["model"];
                $arr["inc"] = 1;
                $existSkuArray[] = $arr;
            }

            foreach ($existSkuArray as &$existSkuValue) {
                $existSku = $existSkuValue["sku"];

                foreach ($this->data as &$product) {

                    $productId = $product[PRODUCT_ID];
                    $oldSku = $product["model"];
                    if ($oldSku == $existSku) {
                        $newSku = $oldSku . "-" . $existSkuValue["inc"];
                        $existSkuValue["inc"] = $existSkuValue["inc"] + 1;

                        $query = "UPDATE oc_product SET model = '$newSku' WHERE product_id = $productId";
//                    sout($query);
                        $this->db->query($query);
                    }
                }
            }
        }
    }

    public function setImagesPath($image) {
        $array = explode("/", $image);
        $firstElemet = reset($array);
        if ($firstElemet == "data") {
            $image = substr($image, 5);
        }
        $image = "images/" . $image;

        return $image;
    }

}
