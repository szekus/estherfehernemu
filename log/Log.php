<?php

namespace log;

/**
 * Description of Log
 *
 * @author szekus
 */
class Log {

    protected $start;
    protected $stop;
    protected $db;
    protected $timeStart;
    protected $timeEnd;
    protected $dateStart;
    protected $dateEnd;
    protected $executionTime;
    protected $data;

    protected function __construct() {
        $this->db = \db\Database::instance();
    }

    
    public static function create() {
        return new Log();
    }
    public function run() {
        
    }

    public function start() {
        $this->timeStart = microtime(true);
        $this->dateStart = (date('Y-m-d H:i:s'));
    }

    public function stop() {
        $this->timeEnd = microtime(true);
        $this->executionTime = ($this->timeEnd - $this->timeStart) / 60;
        echo '<b>Total Execution Time:</b> ' . $this->executionTime . ' Mins';
        $this->dateEnd = (date('Y-m-d H:i:s'));
    }

    public function save() {
        $this->start = $this->dateStart;
        $this->stop = $this->dateEnd;
    }

    public function getExecutionTime() {
        return $this->executionTime;
    }

    function getStart() {
        return $this->start;
    }

    function setStart($start) {
        $this->start = $start;
    }

    function getStop() {
        return $this->stop;
    }

    function setStop($stop) {
        $this->stop = $stop;
    }

}
