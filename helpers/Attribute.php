<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace helpers;

/**
 * Description of Attribute
 *
 * @author szekus
 */
class Attribute extends Helper {

    protected $SRId;
    protected $nev;
    protected $cimke;
    protected $attributeValues;
    protected $attributes;
    protected $productClassArray;
    protected $db;

    function setProductClassArray($productClassArray) {
        $this->productClassArray = $productClassArray;
    }

    function getAttributes() {
        return $this->attributes;
    }

    function getCimke() {
        return $this->cimke;
    }

    function getProductClassArray() {
        return $this->productClassArray;
    }

    function getAttributeValues() {
        return $this->attributeValues;
    }

    function getNev() {
        return $this->nev;
    }

    public static function create() {
        $attribute = new Attribute();
        $attribute->db = \db\Database::instance();
        return $attribute;
    }

    protected function __construct() {
        parent::__construct();
    }

    public function setUp() {
        $this->attributes = array();

        $oneAttribute = Attribute::create();
        $oneAttribute->id = 1;
        $oneAttribute->nev = "Kiszerelés";
        $oneAttribute->cimke = "kiszereles";

        $query = "SELECT id_pl, kiszer, count(*) AS c FROM products_lang WHERE kiszer !='' AND lang = 'hu' GROUP BY kiszer HAVING c > 0";
        $result = $this->db->query($query);

//        sout($res);

        $attributeValueArray = array();
        foreach ($result as $item) {

            $attributeValue = AttributeValue::create();
            $attributeValue->name = $item["kiszer"];
            $attributeValue->id = $item["id_pl"];


            $query = "SELECT id_prod FROM products_lang WHERE lang = 'hu' AND kiszer = '" . $item["kiszer"] . "'";
            $res = $this->db->query($query);

            foreach ($res as $resValue) {
                $attributeValue->productIdArray[] = $resValue["id_prod"];
            }


            $attributeValueArray[] = $attributeValue;
        }
        $oneAttribute->attributeValues = $attributeValueArray;
        $this->attributes[] = $oneAttribute;

        $query = "SELECT * FROM groups_lang WHERE lang = 'hu' AND id_group != 7 ";
        $groups = $this->db->query($query);

        foreach ($groups as $group) {
            $oneAttribute = Attribute::create();
            $oneAttribute->id = $group["id_gl"];
            $oneAttribute->nev = $group["title"];
            $oneAttribute->cimke = slug($group["title"]);

            $query = "SELECT * FROM groups_items_lang AS gil " .
                    "INNER JOIN groups_items AS gi ON "
                    . "gil.id_item = gi.id_item AND "
                    . "lang = 'hu' AND "
                    . "gi.id_group =  " . $group["id_group"];
//            sout($query);
            $group_items = $this->db->query($query);


            $attributeValueArray = array();
            foreach ($group_items as $group_item) {
                $attributeValue = AttributeValue::create();
                $attributeValue->name = $group_item["title"];
                $attributeValue->id = $group_item["id_item"];

                $query = "SELECT id_prod FROM products_group WHERE id_item = " . $group_item["id_item"];
                $res = $this->db->query($query);

                foreach ($res as $resValue) {
                    $attributeValue->productIdArray[] = $resValue["id_prod"];
                }

                $attributeValueArray[] = $attributeValue;
            }
            $oneAttribute->attributeValues = $attributeValueArray;
            $this->attributes[] = $oneAttribute;
        }

        foreach ($this->attributes as $attr) {

            if ($attr->cimke == "kiszereles") {
                $productClassIdArray = array();
                foreach ($attr->attributeValues as $attrValue) {
                    $query = "SELECT some FROM products WHERE id_prod IN (SELECT id_prod FROM products_lang WHERE lang = 'hu' AND kiszer = '" . $attrValue->name . "')";

                    $productClassIdArray = $this->db->query($query);

                    foreach ($this->productClassArray as &$productClass) {
                        foreach ($productClassIdArray as $productClassId) {
                            if ($productClass["original_id"] == $productClassId["some"] && $productClassId["some"] != "0") {
                                if (array_key_exists("first", $productClass) && !array_key_exists("second", $productClass)) {
                                    if ($attr->cimke != $productClass["first"]) {
                                        $productClass["second"] = $attr->cimke;
                                    }
                                } else if (!array_key_exists("first", $productClass)) {
                                    $productClass["first"] = $attr->cimke;
                                }
                            }
                        }
                    }
                }
            } else {
                $productClassIdArray = array();
                foreach ($attr->attributeValues as $attrValue) {

                    $query = "SELECT some FROM products WHERE id_prod IN (SELECT id_prod FROM products_group WHERE id_item = " . $attrValue->id . ") GROUP BY some";

                    $productClassIdArray = $this->db->query($query);

                    foreach ($this->productClassArray as &$productClass) {
                        foreach ($productClassIdArray as $productClassId) {
                            if ($productClass["original_id"] == $productClassId["some"]) {

                                if (array_key_exists("first", $productClass) && !array_key_exists("second", $productClass)) {
                                    if ($attr->cimke != $productClass["first"]) {
                                        $productClass["second"] = $attr->cimke;
                                    }
                                } else if (!array_key_exists("first", $productClass)) {
                                    $productClass["first"] = $attr->cimke;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function createWCOptionArray($productId) {
        $query = "SELECT * FROM  wp_term_taxonomy WHERE taxonomy like 'pa_%' AND term_taxonomy_id IN(SELECT term_taxonomy_id FROM wp_term_relationships WHERE object_id = $productId)";
        $result = $this->db->query($query);
        $attributes = array();
        foreach ($result as $value) {
            $attribute_name = str_replace("pa_", "", $value["taxonomy"]);

            $query = "SELECT attribute_label FROM wp_woocommerce_attribute_taxonomies WHERE attribute_name = '$attribute_name'";
            $attribute_label = $this->db->findOne($query, 'attribute_label');

            $query = "SELECT name FROM wp_terms WHERE term_id = " . $value["term_id"];
            $name = $this->db->findOne($query, 'name');

            $query = "SELECT slug FROM wp_terms WHERE term_id = " . $value["term_id"];
            $slug = $this->db->findOne($query, 'slug');

            $attr = array();

            if ($this->isExistLabel($attributes, $attribute_label)) {
                foreach ($attributes as &$attribute_value) {
                    if ($attribute_value["label"] == $attribute_label) {
                        $attribute_value["name"] .= "||" . $name;
                        $attribute_value["slug"] .= "||" . $slug;
                    }
                }
            } else {
                $attr["label"] = $attribute_label;
                $attr["name"] = $name;
                $attr["slug"] = $slug;
            }

            if (count($attr) > 0) {
                $attributes[] = $attr;
            }
        }

        return $attributes;
    }

    public function getWCProductChildrenPriceAttribute($productId) {
        $children = array();

        $query = "SELECT * FROM wp_options WHERE option_name like '_transient_wc_product_children_$productId'";
        $result = $this->db->query($query);
        if (count($result) > 0) {
            $childIds = unserialize($result[0]["option_value"]);
            foreach ($childIds["all"] as $childId) {
                $query = "SELECT meta_value FROM wp_postmeta WHERE meta_key like '%attribute_pa%' AND post_id = $childId";
                $name = $this->db->findOne($query, "meta_value");

                $query = "SELECT meta_value FROM wp_postmeta WHERE meta_key = '_price' AND post_id = $childId";
                $price = $this->db->findOne($query, "meta_value");


                $query = "SELECT meta_key FROM wp_postmeta WHERE meta_key like '%attribute_pa%' AND post_id = $childId";
                $label = $this->db->findOne($query, "meta_key");
                $label = str_replace("attribute_pa_", "", $label);

                $arr = array();
                $arr["child_attribute_label"] = $label;
                $arr["child_attribute_name"] = $name;
                $arr["child_attribute_price"] = $price;
                $children[] = $arr;
            }
        }

        return $children;
    }

    public function createWCAttributesString($productId) {

        $attributes = $this->createWCOptionArray($productId);
        $str = "";
        foreach ($attributes as $value) {
//            var_dump($value);
            $str .= $value["label"] . ": " . $value["name"] . "<br>";
        }
        $str = str_replace("||", ", ", $str);
        return $str;
//        sout("---------");
    }

    private function isExistLabel($array, $label) {
        $bool = false;
        foreach ($array as $value) {
            if (isset($value["label"]) && $value["label"] == $label) {
                return true;
            }
        }
        return $bool;
    }

}
