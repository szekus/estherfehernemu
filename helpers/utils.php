<?php

function pre_print($printable) {
    echo '<pre>';
    print_r($printable);
    echo '</pre>';
}

function slug($string, $space = "-") {
    // $string = utf8_encode($string);

    /*
      if (function_exists('iconv')) {
      $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
      } */
    $string = str_replace(['Á', 'É', 'Í', 'Ő', 'Ú', 'Ó', 'Ö', 'Ű', 'Ü', 'í', 'ü', 'ó', 'ő', 'ú', 'á', 'é', 'ű', 'ö', "ú"], ['A', 'E', 'I', 'O', 'U', 'O', 'O', 'U', 'U', 'i', 'u', 'o', 'o', 'u', 'a', 'e', 'u', 'o', "u"], $string);
    $string = preg_replace("/[^a-zA-Z0-9 \-]/", "", $string);
    $string = preg_replace('/-/', ' ', $string);
    $string = trim(preg_replace("/\\s+/", " ", $string));
    $string = strtolower($string);
    $string = str_replace(" ", $space, $string);

    return $string;
}

function bugged_slug2($string, $space = "-") {
    // $string = utf8_encode($string);

    /*
      if (function_exists('iconv')) {
      $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
      } */
    $string = str_replace(['Á', 'É', 'Í', 'Ő', 'Ú', 'Ó', 'Ö', 'Ű', 'Ü', 'í', 'ü', 'ó', 'ő', 'ú', 'á', 'é', 'ű', 'ö', "ú"], ['A', 'E', 'I', 'O', 'U', 'O', 'O', 'U', 'U', 'i', 'u', 'o', 'o', 'u', 'a', 'e', 'u', 'o', "u"], $string);
    $string = preg_replace("/[^a-zA-Z0-9 \-]/", "", $string);
//    $string = preg_replace('/-/', ' ', $string);
//    $string = trim(preg_replace("/\\s+/", " ", $string));
//    $string = strtolower($string);
    $string = str_replace(" ", $space, $string);

    return $string;
}

function bugged_slug($string, $space = "-") {
    // $string = utf8_encode($string);

    /*
      if (function_exists('iconv')) {
      $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
      } */
    $string = str_replace(['Á', 'É', 'Í', 'Ő', 'Ú', 'Ó', 'Ö', 'Ű', 'Ü', 'í', 'ü', 'ó', 'ő', 'ú', 'á', 'é', 'ű'], ['A', 'E', 'I', 'O', 'U', 'O', 'O', 'U', 'U', 'i', 'u', 'o', 'o', 'u', 'a', 'e', 'u'], $string);
    $string = preg_replace("/[^a-zA-Z0-9 \-]/", "-", $string);
    $string = preg_replace('/-/', ' ', $string);
//    $string = trim(preg_replace("/\\s+/", " ", $string));
//    $string = strtolower($string);
    $string = str_replace(" ", $space, $string);

    return $string;
}

function bolthelyOptionsExplode($optionsRow) {
    $options = [];
    $propparts = explode('*', $optionsRow);
    if (is_array($propparts) && count($propparts) > 0) {
        $propname = array_shift($propparts);
        foreach ($propparts as $prop) {
            $options[$propname][$prop] = $prop; // 1 prop egyszer
        }
    }
    return $options;
}

function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function split_name($string) {
    $arr = explode(' ', $string);
    $count = count($arr);
    $firsName = "";
    $lastName = "";
    if ($count == 2) {
        $firsName = $arr[0];
        $lastName = $arr[1];
    } else {
        $firsName = $arr[0];
        for ($i = 1; $i < $count; $i++) {
            $lastName .= $arr[$i] . " ";
        }
    }

    $lastName = trim($lastName);
    $firsName = trim($firsName);
    if ($lastName == "") {
        $lastName = "Nincs megadva";
    }
    if ($firsName == "") {
        $firsName = "Nincs megadva";
    }
    return array("firstname" => $firsName, "lastname" => $lastName);
}

function getNetPrice($price, $tax) {
    if ($price == "") {
        $price = 0;
    }
    return $price / (1.00 + $tax / 100);
}

function getGrossPrice($price, $tax) {
    if ($price == "") {
        $price = 0;
    }
    return ( $price * (1.00 + $tax / 100));
}

function sout($param, $die = false) {
    echo '<pre>';
    echo "\n";
    var_dump($param);
    echo "\n";
    if ($die) {
        die();
    }
}

function getId($url) {
    $arr = explode("/", $url);
    return $arr[count($arr) - 1];
}

function getParamValue($url, $param) {
    $parts = parse_url($url);
    parse_str($parts['query'], $query);
    return $query[$param];
}

function removeAccent($string) {
    $from = array("Á", "É", "Í", "Ó", "Ö", "Ő", "Ú", "Ü", "Ű", "Ó", "O", "U", "Ü", "á", "é", "í", "ó", "ö", "ő", "ú", "ü", "ű", "ó");
    $to = array("A", "E", "I", "O", "O", "O", "U", "U", "U", "O", "O", "U", "U", "a", "e", "i", "o", "o", "o", "u", "u", "u", "o");
    $string = str_replace($from, $to, $string);
    return $string;
}

function createArrayFromCSV($fileUrl) {
    $array = array_map('str_getcsv', file($fileUrl));
    $header = explode(";", $array[0][0]);
//    sout($array);
    $result = array();
    for ($i = 1; $i < count($array); $i++) {
        $data = array();
        $tmp = explode(";", $array[$i][0]);
//        sout($tmp);
//        die();
        for ($j = 0; $j < (count($header)); $j++) {
            $data[$header[$j]] = $tmp[$j];
        }
        $result[] = $data;
    }

    return $result;
}

function getApiEndpointFromHref($href) {
    $arr = explode("/", $href);
    return $arr[count($arr) - 1];
}

function checkAndReturnValue($array, $key) {
    if (array_key_exists($key, $array)) {
        return $array[$key];
    } else {
        return returnDefaultValue($key);
    }
}

function returnDefaultValue($key) {
    switch ($key) {
        case "billing_country":
        case "shipping_country":
            return "HU";
        case "billing_phone":
            return 1;
        default:
            return "";
    }
}

function isEmpty($value) {
    if ($value == "" || $value == NULL) {
        return true;
    }
}

function get_string_between($string, $start, $end) {
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0)
        return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function parseHtml($param) {
    $param = str_replace("[felsorol]", "<ul>", $param);
    $param = str_replace("[/felsorol]", "</ul>", $param);

    $param = str_replace("[elem]", "<li>", $param);
    $param = str_replace("[/elem]", "</li>", $param);

    $param = str_replace("[vonal]", "<hr>", $param);
    $param = str_replace("[terkoz]", "<br>", $param);

    $param = str_replace("[f]", "<b>", $param);
    $param = str_replace("[/f]", "</b>", $param);

    $param = str_replace("[b]", "<b>", $param);
    $param = str_replace("[/b]", "</b>", $param);

    $param = str_replace("[d]", "<i>", $param);
    $param = str_replace("[/d]", "</i>", $param);

    $param = str_replace("[tablazat]", "<tr>", $param);

    for ($i = 2; $i < 11; $i++) {
        $param = str_replace("[tablazat $i]", "<tr>", $param);
    }

    for ($i = 2; $i < 11; $i++) {
        $param = str_replace("[cella $i]", "", $param);
    }
    $param = str_replace("[/tablazat]", "</tr>", $param);


    $param = str_replace("[cella]", "<td>", $param);
    $param = str_replace("[/cella]", "</td>", $param);

    $param = str_replace("[a]", "<i>", $param);
    $param = str_replace("[/a]", "</i>", $param);

    $param = str_replace("[kozep]", "<h4>", $param);
    $param = str_replace("[/kozep]", "</h4>", $param);

    $param = str_replace("[kieg]", "", $param);
    $param = str_replace("[/kieg]", "", $param);
    $param = str_replace("[cella /]", "", $param);
    $param = str_replace("[cella ]", "", $param);

    $param = str_replace("[nagycim]", "<h3>", $param);
    $param = str_replace("[/nagycim]", "</h3>", $param);



    $param = str_replace("[cim]", "<h5>", $param);
    $param = str_replace("[/cim]", "</h5>", $param);

    $matches = array();
    preg_match_all("/\[\bkisablak\b (.*?)\[\/\bkisablak\b\]/", $param, $matches);

    if (count($matches[0]) > 0) {
        foreach ($matches[0] as $match) {
            $param = str_replace($match, "", $param);
        }
    }
    $param = str_replace("[/kisablak]", "", $param);

    $param = str_replace("[kiemel]", "<b>", $param);
    $param = str_replace("[/kiemel]", "</b>", $param);

    $param = str_replace("[/vonal]", "</b>", $param);
    $param = str_replace("[/terkoz]", "</b>", $param);


//    $param = str_replace("[kisablak t-heart][kep]t-heart.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak t-pulse][kep]t-pulse.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak t-ergo-meter][kep]t-ergo-meter.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak t-scale][kep]t-scale.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak t-user-req][kep]t-user-req.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak t-heartspeed][kep]t-heartspeed.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak t-ownkcal][kep]t-ownkcal.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak t-race][kep]t-race.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak t-fit-test][kep]t-fit-test.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak backlite][kep]icon_backlite.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak ems][kep]icon_ems.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak directdrive][kep]icon_directdrive.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak swingarm][kep]icon_swingarm.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak t-fold][kep]t-fold.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak t-psc][kep]t-psc.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak t-flex][kep]t-flex.png[/kep][/kisablak]", "", $param);
//    $param = str_replace("[kisablak motioncontrol][kep]icon_motioncontrol.png[/kep][/kisablak] ", "", $param);
//    $param = str_replace("[kisablak easyfold][kep]icon_easyfold.png[/kep][/kisablak] ", "", $param);
//    $param = str_replace("[kisablak motioncontrol][kep]icon_motioncontrol.png[/kep][/kisablak] ", "", $param);

    $param = str_replace("[/link]", "</a>", $param);
    $matches = array();
    preg_match('/\[\blink\b (.*?)\]/', $param, $matches);
    if (count($matches) > 0) {
        $a_href = "<a href='" . $matches[1] . "' target='_blank'>";
        $param = str_replace($matches[0], $a_href, $param);
    }
//    $param = str_replace("[/hasonlo]", "[hasonlo]", $param);
//    preg_match("/\[\bhasonlo\b\](.*?)\[\\\bhasonlo\b\]/", $param, $matches);
    preg_match_all("/\[\bhasonlo\b\](.*?)\[\/\bhasonlo\b\]/", $param, $matches);



    if (count($matches[0]) > 0) {
        foreach ($matches[0] as $match) {
            $param = str_replace($match, "", $param);
        }
    }








    $param = str_replace("[/kisablak]", "", $param);

    return $param;
}

function getCatId($anotherCategories, $mainCategoryName, $count) {
//    sout($mainCategoryName);
    $id = 0;
    foreach ($anotherCategories as $ac) {


        if ($mainCategoryName === "") {
            return 0;
        }
        if ($ac["name"] === $mainCategoryName) {
            if ($count == 0) {
                $id = $ac["id"];
            } else {
                $count--;
                continue;
            }

//            sout($ac);
        }
    }

    return $id;
}

function formatTaxNumber($taxNumber) {

    $numbers = (string) preg_replace("/[^0-9]/", '', $taxNumber);
    $formattedTaxNumber = "";

    if ($numbers != "") {
        $array = array_map('intval', str_split($numbers));

        $part_1 = "";
        $part_2 = "";
        $part_3 = "";

        $counter = 1;
        foreach ($array as $number) {

            if ($counter <= 8) {
                $part_1 .= $number;
            }

            if ($counter == 9) {
                $part_2 .= $number;
            }


            if ($counter == 10 || $counter == 11) {
                $part_3 .= $number;
            }
            ++$counter;
        }


        if (strlen($part_1) <= 8 && strlen($part_1) > 0) {
            $formattedTaxNumber = $part_1;
        }
        if (strlen($part_2) == 1) {
            $formattedTaxNumber .= "-" . $part_2;
        }
        if (strlen($part_3) == 2 || strlen($part_3) == 1) {
            $formattedTaxNumber .= "-" . $part_3;
        }
    }

    return $formattedTaxNumber;
}
