<?php

namespace helpers;

class EMOrderXML extends Helper {

    protected $id;
    protected $sr_order_id;
    protected $em_order_id;
    protected $status;
    protected $request_xml;
    protected $response_xml;
    protected $resend;

    protected function __construct() {
        parent::__construct();
    }

    public static function create() {
        $emOrderXML = new EMOrderXML();
        return $emOrderXML;
    }

    public function insert() {
        $query = "SELECT * FROM em_order_xml WHERE em_order_id = '$this->em_order_id'";
        
        if (!$this->db->isExist($query)) {
            $query = "INSERT INTO em_order_xml (`sr_order_id`, `em_order_id`, `status`, `request_xml`) 
                  VALUES($this->sr_order_id,$this->em_order_id,0,'$this->request_xml')";

            $this->db->query($query);
        }
    }

    public function findByEMId($em_order_id) {

        $this->db->query($query);
    }

    public function getAll() {
        $query = "SELECT * FROM em_order_xml";
        $result = $this->db->query($query);

        $resultArray = array();
        foreach ($result as $value) {
            $emOrderXML = EMOrderXML::create();
            $emOrderXML->setId($value["id"]);
            $emOrderXML->setEm_order_id($value["em_order_id"]);
            $emOrderXML->setSr_order_id($value["sr_order_id"]);
            $emOrderXML->setRequest_xml($value["request_xml"]);
            $emOrderXML->setResponse_xml($value["response_xml"]);
            $emOrderXML->setStatus($value["status"]);
            $emOrderXML->setResend($value["resend"]);

            $resultArray[] = $emOrderXML;
        }

        return $resultArray;
    }

    public function update() {

        $query = "
            UPDATE em_order_xml 
            SET em_order_id=$this->em_order_id,
                sr_order_id=$this->sr_order_id,
                request_xml='$this->request_xml',
                response_xml='$this->response_xml',
                status=$this->status,
                resend=$this->resend
            WHERE id = $this->id                    
                ";
//        sout($query);
        $this->db->query($query);
    }

    function getSr_order_id() {
        return $this->sr_order_id;
    }

    function getEm_order_id() {
        return $this->em_order_id;
    }

    function getStatus() {
        return $this->status;
    }

    function setSr_order_id($sr_order_id) {
        $this->sr_order_id = $sr_order_id;
    }

    function setEm_order_id($em_order_id) {
        $this->em_order_id = $em_order_id;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function getRequest_xml() {
        return $this->request_xml;
    }

    function setRequest_xml($request_xml) {
        $this->request_xml = $request_xml;
    }

    function getResponse_xml() {
        return $this->response_xml;
    }

    function setResponse_xml($response_xml) {
        $this->response_xml = $response_xml;
    }

    function getId($id = null) {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function getResend() {
        return $this->resend;
    }

    function setResend($resend) {
        $this->resend = $resend;
    }

}
