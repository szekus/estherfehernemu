<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace helpers;

/**
 * Description of CountryHelper
 *
 * @author szekus
 */
class CountryHelper extends Helper {

    protected $dbCountries;
    protected $SRCountries;

    public function __construct() {
        parent::__construct();
        $query = "SELECT * FROM aaasr_countries";
        $result = $this->db->query($query);
        foreach ($result as $value) {
            $this->countries[] = $value;
        }
    }

    public static function create() {
        $countryHelper = new CountryHelper();
        return $countryHelper;
    }

    function getCountries() {
        return $this->countries;
    }

    function setCountries($countries) {
        $this->countries = $countries;
    }

    public function createCountryId($isoCode2) {

        foreach ($this->countries as $country) {
            if ($country["country_3_code"] === $isoCode2) {
                return base64_encode("country-country_id=" . $country["country_id"]);
            }
        }
    }

    public function getSRCountryIdByIsoCode2($isoCode2) {
        foreach ($this->SRCountries as $SRCountry) {
            if ($SRCountry["isoCode2"] == $isoCode2) {
                return $SRCountry["id"];
            }
        }
        return "";
    }

    public function getSRCountryById($id) {
        foreach ($this->countries as $country) {
            $countrySRID = base64_encode("country-country_id=" . $id);
            if ($country["id"] === $countrySRID) {
                return $country["id"];
            }
        }
        return base64_encode("country-country_id=97");
    }

    public function getSRCountries() {
        $result = querySRApi(API_ENDPOINT_COUNTRIES . "?limit=200&page=0", [], 'GET', 'responseBody', false);

        foreach ($result["items"] as $item) {
            $SRID = $this->getId($item["href"]);
            $res = querySRApi(API_ENDPOINT_COUNTRIES . "/" . $SRID, [], 'GET', 'responseBody', false);
            $SRcountry["id"] = $res["id"];
            $SRcountry["name"] = $res["name"];
            $SRcountry["isoCode2"] = $res["isoCode2"];
            $SRcountry["isoCode3"] = $res["isoCode3"];
            $SRcountry["status"] = $res["status"];
            $SRcountry["zones"] = $res["zones"];

            $this->SRCountries[] = $SRcountry;
        }
        $result = querySRApi(API_ENDPOINT_COUNTRIES . "?limit=200&page=1", [], 'GET', 'responseBody', false);

        foreach ($result["items"] as $item) {
            $SRID = $this->getId($item["href"]);
            $res = querySRApi(API_ENDPOINT_COUNTRIES . "/" . $SRID, [], 'GET', 'responseBody', false);
            $SRcountry["id"] = $res["id"];
            $SRcountry["name"] = $res["name"];
            $SRcountry["isoCode2"] = $res["isoCode2"];
            $SRcountry["isoCode3"] = $res["isoCode3"];
            $SRcountry["status"] = $res["status"];
            $SRcountry["zones"] = $res["zones"];

            $this->SRCountries[] = $SRcountry;
        }
    }

}
