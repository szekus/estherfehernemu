<?php

namespace helpers;

/**
 * Description of ManufacturerHelper
 *
 * @author tamas
 */
class ManufacturerHelper extends Helper {

    protected $manufacturers;

    function setManufacturers() {
        $this->manufacturers = $this->db->query(MANUFACTURER_QUERY);
    }

    function getManufacturers() {
        return $this->manufacturers;
    }

    protected function __construct() {
        parent::__construct();
    }

    public static function create() {
        $manufacturerHelper = new ManufacturerHelper();
        return $manufacturerHelper;
    }

    public function insertManufacturer() {
        $request = \resources\Request::create();
        foreach ($this->manufacturers as $manufacturerValue) {

            $manufacturerId = $manufacturerValue[MANUFACTURER_ID];
            $manufacturerSRId = base64_encode("manufacturer-manufacturer_id=" . $manufacturerId);

            $ManufacturerResource = \resources\Manufacturer::create();
            $ManufacturerResource->id = $manufacturerId;
            $ManufacturerResource->innerId = $manufacturerId;
            $ManufacturerResource->name = $manufacturerValue["name"];
            $ManufacturerResource->sortOrder = 0;
            $ManufacturerResource->picture = "images/" . $manufacturerValue["image"];

            $ManufacturerResource->setPathInsert();
            $request->addBatch($ManufacturerResource->createBatchArray());
        }
        $request->run(true);
    }

    public function insertManufacturerDescription() {
        $request = \resources\Request::create();
        foreach ($this->manufacturers as $manufacturerValue) {

            $manufacturerId = $manufacturerValue[MANUFACTURER_ID];
            $manufacturerSRId = base64_encode("manufacturer-manufacturer_id=" . $manufacturerId);


            $manufacturerDescription = \resources\ManufacturerDescription::create();
            $manufacturerDescription->language = array("id" => LANG_HU);
            $manufacturerDescription->manufacturer = array("id" => $manufacturerSRId);
            $manufacturerDescription->description = $manufacturerValue["description"];
            $manufacturerDescription->metaDescription = $manufacturerValue["meta_description"];
            $manufacturerDescription->metaKeywords = $manufacturerValue["meta_keyword"];

            $manufacturerDescription->setPathInsert();
//            $manufacturerDescription->insert(true);
            $request->addBatch($manufacturerDescription->createBatchArray());
        }
        $request->run(true);
    }

    public function deleteManufacturer() {
        $list = array("m_r");
        $deleteResource = \helpers\DeleteResource::create();
        $deleteResource->setIsDelete(true);
        $deleteResource->setList($list);
        $deleteResource->deleteList();
    }

}
