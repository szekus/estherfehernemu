<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace helpers;

/**
 * Description of DeleteHelper
 *
 * @author tamas
 */
class DeleteResource {

    protected $list = array();
    protected $isDelete = false;

    function getList() {
        return $this->list;
    }

    function setList($list) {
        $this->list = $list;
    }

    public function addList($element) {
        $this->list[] = $element;
    }

    function getIsDelete() {
        return $this->isDelete;
    }

    function setIsDelete($isDelete) {
        $this->isDelete = $isDelete;
    }

    protected function __construct() {
        
    }

    public static function create() {
        $deleteResource = new DeleteResource();
        return $deleteResource;
    }

    public function deleteList() {

        if ($this->isDelete) {

            foreach ($this->list as $element) {
                $this->deleteResource($element);
            }
        }
    }

    private function deleteResource($resourceName) {

        switch ($resourceName) {
            case "coupon":
                $couponResource = \resources\Coupon::create();
                $couponResource->deleteAll();
                break;
            case "coupon_description":
                $couponDescriptionTagResource = \resources\CouponDescription::create();
                $couponDescriptionTagResource->deleteAll();
                break;
            case "coupon_product_relation":
                $couponProductRealtionResource = \resources\CouponProductRelation::create();
                $couponProductRealtionResource->deleteAll();
                break;
            case "coupon_category_relation":
                $couponCategoryRelationResource = \resources\CouponCategoryRelation::create();
                $couponCategoryRelationResource->deleteAll();
                break;
            case "p_t_r":
                $productTagResource = \resources\ProductTag::create();
                $productTagResource->deleteAll();
                break;
            case "p_l_a_v_r_r":
                $productListAttributeValueRelationResource = \resources\ResourceFactory::getInstance()->build("p_l_a_v_r_r");
                $productListAttributeValueRelationResource->deleteAll();
                break;
            case "o_s":
                $orderStatusResource = \resources\ResourceFactory::getInstance()->build("o_s");
                $orderStatusResource->deleteAll();
                break;
            case "l_a_v_r":
                $listAttributeValueResource = \resources\ResourceFactory::getInstance()->build("l_a_v_r");
                $listAttributeValueResource->deleteAll();
                break;
            case "p_class_a_r_r":
                $productClassAttributeRelationResource = \resources\ResourceFactory::getInstance()->build("p_class_a_r_r");
                $productClassAttributeRelationResource->deleteAll();
                break;
            case "l_a_r":
                $listAttributeResource = \resources\ResourceFactory::getInstance()->build("l_a_r");
                $listAttributeResource->deleteAll();
                break;
            case "a_d_r":
                $attributeDescriptionResource = \resources\ResourceFactory::getInstance()->build("a_d_r");
                $attributeDescriptionResource->deleteAll();
                break;
            case "cus_r":
                $customerResource = \resources\ResourceFactory::getInstance()->build("cus_r");
                $customerResource->deleteAll();
                break;
            case "cus_group":
                $customerGroupResource = \resources\CustomerGroup::create();
                $customerGroupResource->deleteAll();
                break;
            case "a_r":
                $addressResource = \resources\ResourceFactory::getInstance()->build("a_r");
                $addressResource->deleteAll();
                break;
            case "p_cat_r":
                $productCategoryRelation = \resources\ResourceFactory::getInstance()->build("p_cat_r");
                $productCategoryRelation->deleteAll();
                break;
            case "p_o_r":
                $productOptionResource = \resources\ResourceFactory::getInstance()->build("p_o_r");
                $productOptionDescriptionResource = \resources\ResourceFactory::getInstance()->build("p_o_d_r");
                $productOptionValueResource = \resources\ResourceFactory::getInstance()->build("p_o_v_r");
                $productOptionValueDescriptionResource = \resources\ResourceFactory::getInstance()->build("p_o_v_d_r");

                $productOptionResource->deleteAll();
                $productOptionDescriptionResource->deleteAll();
                $productOptionValueResource->deleteAll();
                $productOptionValueDescriptionResource->deleteAll();
                break;
            case "p_r":
                $productResource = \resources\ResourceFactory::getInstance()->build("p_r");
                $productResource->deleteAll();
                break;
            case "p_d_r":
                $productDescriptionResource = \resources\ResourceFactory::getInstance()->build("p_d_r");
                $productDescriptionResource->deleteAll();
                break;
            case "p_s_r":
                $productSpecialResource = \resources\ResourceFactory::getInstance()->build("p_s_r");
                $productSpecialResource->deleteAll();
                break;
            case "p_class_r":
                $productClassResource = \resources\ResourceFactory::getInstance()->build("p_class_r");
                $productClassResource->deleteAll();
                break;
            case "p_rel_p_r_r":
                $productRelatedProductRelation = \resources\ResourceFactory::getInstance()->build("p_rel_p_r_r");
                $productRelatedProductRelation->deleteAll();
                break;
            case "p_col_p_r_r":
                $productCollateralProductRelation = \resources\ResourceFactory::getInstance()->build("p_col_p_r_r");
                $productCollateralProductRelation->deleteAll();
                break;
            case "p_p_b_r_r":
                $productProductBadgeRelationResource = \resources\ResourceFactory::getInstance()->build("p_p_b_r_r");
                $productProductBadgeRelationResource->deleteAll();
                break;
            case "u_a_r_prod":
                $urlAlias = \resources\ResourceFactory::getInstance()->build("u_a_r");
                $urlAlias->deleteProductUrl();
                break;
            case "u_a_r_cat":
                $urlAlias = \resources\ResourceFactory::getInstance()->build("u_a_r");
                $urlAlias->deleteCategoryUrl();
                break;
            case "m_r":
                $manufacturerResource = \resources\ResourceFactory::getInstance()->build("m_r");
                $manufacturerResource->deleteAll();
                break;
            case "cat_r":
                $categoryResource = \resources\ResourceFactory::getInstance()->build("cat_r");
                $categoryResource->deleteAll();
                break;
//            case "u_r":
//                $categoryResource = \resources\ResourceFactory::getInstance()->build("u_r");
//                $categoryResource->deleteAll();
//                break;
            case "p_i_r":
                $productImageResource = \resources\ResourceFactory::getInstance()->build("p_i_r");
                $productImageResource->deleteAll();
                break;
            case "l_p":
                $loyalPointResource = \resources\ResourceFactory::getInstance()->build("l_p");
                $loyalPointResource->deleteAll();
                break;
            case "cat_desc":
                $categoryDescriptionResource = \resources\ResourceFactory::getInstance()->build("cat_desc");
                $categoryDescriptionResource->deleteAll();
                break;
            default:
                break;
        }
    }

}
