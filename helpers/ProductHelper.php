<?php

namespace helpers;

ini_set("memory_limit", "-1");

use resources;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductHelper
 *
 * @author szekus
 */
class ProductHelper extends Helper {

    function setProducts($debug = false) {

        $productDataSetter = \datasetter\ProductDataSetter::create();
        $productDataSetter->setData();
        $this->products = $productDataSetter->getData();

        if ($debug) {
            sout($this->products);
            die();
        }
    }

    protected $products;
    protected $productClass;
    protected $childProducts;
    protected $manufacturers;
    protected $categories;
    public $parentProducts;
    public $productCollection;
    protected $productClassFSParams = array();
    protected $listAttributes;

    protected function __construct() {
        parent::__construct();
        $this->limit = -1;
    }

    function getProductCollection() {
        return $this->productCollection;
    }

    function getProductClassFSParams() {
        return $this->productClassFSParams;
    }

    function getListAttributes() {
        return $this->listAttributes;
    }

    function setProductCollection($productCollection) {
        $this->productCollection = $productCollection;
    }

    function setProductClassFSParams($productClassFSParams) {
        $this->productClassFSParams = $productClassFSParams;
    }

    function setListAttributes($listAttributes) {
        $this->listAttributes = $listAttributes;
    }

    public static function create() {
        $productHelper = new ProductHelper();
        return $productHelper;
    }

    function getManufacturers() {
        return $this->manufacturers;
    }

    function setManufacturers($manufacturers) {
        $this->manufacturers = $manufacturers;
    }

    function getCategories() {
        return $this->categories;
    }

    function setCategories($categories) {
        $this->categories = $categories;
    }

    function getProducts() {
        return $this->products;
    }

    function getChildProducts() {
        return $this->childProducts;
    }

    function getProductClass() {
        return $this->productClass;
    }

    function setParentProducts() {
        $query = "SELECT * FROM some_codes";
        $some_codes = $this->db->query($query);

        foreach ($some_codes as $value) {

            $parentId = PARENT_ID_NUMBER_ADD + intval($value["id"]);

            $sku = "productGroup-" . $parentId;
            $productResource = \resources\Product::create();
            $productSRId = $productResource->createSRId($parentId);

            $productResource->sku = $sku;
            $productResource->id = $productSRId;
            $productResource->innerId = $parentId;
            $this->parentProducts[] = $productResource;
//            
//            
//            $this->parentProducts[] = $productResource->getBySRId($sku);
        }
    }

    function setProductClass() {
        $query = "SELECT * FROM aaasr_productclasses";
        $result = $this->db->query($query);
        $array = array();
        foreach ($result as $value) {
            $array[] = $value;
        }

        for ($i = 0; $i < count($array); $i++) {
            $class = array();
            $class = $array[$i];

            $name = $array[$i]["name"];
            $arr = explode("-", $name);
            $original_id = trim($arr[count($arr) - 1]);
            $class["original_id"] = $original_id;

            $this->productClass[] = $class;
        }
    }

    function setChildProducts($childProducts) {
        foreach ($products as $child) {
            $childProductSRId = base64_encode("product-product_id=" . $child["id_product"]);
            $productResource = resources\Product::create();
            $child = $productResource->getBySRId($childProductSRId);
            $this->childProducts[] = $child;
        }
    }

    public function createParentProducts() {
        $query = "SELECT * FROM some_codes";
        $parentProducts = $this->db->query($query);
        foreach ($parentProducts as $parent) {
            $productResource = \resources\Product::create();



            $productId = intval($parent["id"]);
            $productId = PARENT_ID_NUMBER_ADD + $productId;
            $productResource->id = $productId;
            $productResource->innerId = $productId;
            $productResource->sku = "productGroup-" . $productId;

            $productResource->stock1 = 100;
            $productResource->orderable = 1;
            $productResource->subtractStock = 1;
            $productResource->status = 1;
            $productResource->price = 1;

            $productResource->taxClass = array("id" => TAX_27_CLASS);

            $productResource->insert(true);

            $productSRId = $productResource->createSRId($productId);

            $query = "SELECT id_categ FROM products_categ " .
                    "WHERE id_prod IN (" .
                    "SELECT p.id_prod FROM products AS p WHERE p.some = " . $parent["id"] . ") " .
                    "GROUP BY id_categ";

            $categories = $this->db->query($query);

            foreach ($categories as $cat) {
                $categoryId = $cat["id_categ"];

                $categorySRId = base64_encode("category-category_id=" . $categoryId);
                $productCategoryRelationResource = resources\ProductCategoryRelation::create();
                $productCategoryRelationResource->product = array('id' => $productSRId);
                $productCategoryRelationResource->category = array('id' => $categorySRId);

//                sout($productCategoryRelationResource->getData());
                $productCategoryRelationResource->insert(true);
//                $this->request->addBatch($productCategoryRelationResource->getData());
            }


            $productDescritionResource = resources\ProductDescription::create();
            $productDescritionResource->name = $parent["title"];
            $productDescritionResource->metaKeywords = "";
            $productDescritionResource->metaDescription = "";
            $productDescritionResource->shortDescription = "";
            $productDescritionResource->description = "";
            $productDescritionResource->product = array("id" => $productSRId);
            $productDescritionResource->language = array("id" => LANG_HU);

            $productDescritionResource->insert(true);
//            $this->request->addBatch($productDescritionResource->getData());
        }
    }

    public function createChildProduct() {
        foreach ($this->products as $product) {

            $some = $product["some"];

            if ($some != "0" && $some != 0) {
                $productResource = new resources\Product();

                $childId = $product["id_prod"];
                $childProductSRId = $productResource->createSRId($childId);

                $parentId = PARENT_ID_NUMBER_ADD + intval($some);
                $parentSku = "productGroup-" . $parentId;

//                $productParentSRId = $productResource->getProductIdBySku($parentSku);
                $productParentSRId = $productResource->createSRId($parentId);

                $productResource->id = $childProductSRId;
                $productResource->parentProduct = array("id" => $productParentSRId);

                $productResource->update(true);
            }
        }
    }

    public function insertProduct($debug = false) {
        $request = resources\Request::create();
        $taxClassResource = resources\TaxClass::create();
        $weightClassResource = \resources\WeightClass::create();
        $manufacturerResource = \resources\Manufacturer::create();

        foreach ($this->products as $product) {
            $productResource = \resources\Product::create();

            $productId = $product[PRODUCT_ID];
            $productSRId = $productResource->createSRId($productId);

            $productResource->id = $productId;
            $productResource->innerId = $productId;

            $sku = $product["model"];
            if ($sku == "") {
                $sku = $productId;
            }

            $productResource->sku = $sku;

            $productResource->quantity = $product["quantity"];


            $productResource->stock1 = $product["quantity"];
            $productResource->subtractStock = 1;

            $productResource->inStockStatus = array("id" => $product["inStockStatus"]);

            $productResource->orderable = 1;


            $productResource->status = $product["status"];

//            $productResource->width = $product["width"];
//            $productResource->height = $product["height"];
//            $productResource->length = $product["length"];
            $productResource->weight = $product["weight"];

            $productResource->price = $product["price"];
            $productResource->taxClass = array("id" => $product["taxClass"]);

            $productResource->minimalOrderNumber = $product["minimum"];


            $id_manufacturer = $product["manufacturer_id"];
            if ($id_manufacturer != 0) {
                $manufacturerSRId = base64_encode("manufacturer-manufacturer_id=" . $id_manufacturer);
                $productResource->manufacturer = array("id" => $manufacturerSRId);
            };

            $productResource->mainPicture = $product["mainPicture"];
//            $productResource->imageAlt = $product["imageAlt"];

            $productResource->productClass = array("id" => $product["productClass"]);

            $productResource->setPathInsert();
            $request->addBatch($productResource->createBatchArray());
//               
        }

        $request->run($debug);
    }

    public function insertProductParent($debug = false) {
        $request = resources\Request::create();
        foreach ($this->products as $product) {
            $productResource = \resources\Product::create();
            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=" . $productId);
            $productResource->id = $productSRId;

            if ($product[PRODUCT_PARENT_ID] != "0") {
                $productParentId = $product[PRODUCT_PARENT_ID];
                $productResource->parentProduct = array("id" => base64_encode("product-product_id=" . $productParentId));

//                            sout($productResource->getData());
//                $productResource->update(true);
                $productResource->setPathUpdate();
                $request->addBatch($productResource->createBatchArray());
            }
        }
        $request->run($debug);
    }

    public function insertProductClass($debug = false) {

        $request = resources\Request::create();
        $productClassArray = $this->db->query(PRODUCT_CLASS_QUERY);
        foreach ($productClassArray as $productClassValue) {

            $productClassId = $productClassValue[PRODUCT_CLASS_ID];
            $productClassSRId = slug($productClassValue["name"]) . "_" . $productClassId;

            $productClass = resources\ProductClass::create();
            $productClass->id = $productClassSRId;
            $productClass->name = $productClassValue["name"];
            $productClass->description = "";
//            $productClass->insertWithOuterId();

            $productClass->setPathUpdate();
            $request->addBatch($productClass->createBatchArray());
        }
        $request->run($debug);
    }

    public function updateProductProductClass($debug = false) {

        $request = \resources\Request::create();
        foreach ($this->products as $product) {
            $productResource = \resources\Product::create();
            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=" . $productId);
            $productResource->id = $productSRId;

            if ($product["productClass"] != "") {
                $productResource->productClass = array("id" => $product["productClass"]);
                $productResource->setPathUpdate();
                $request->addBatch($productResource->createBatchArray());
            }
        }
        $request->run($debug);
    }

    public function updateProductWeight() {
        foreach ($this->products as $product) {
            $productResource = \resources\Product::create();

            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=" . $productId);

            $productResource->id = $productSRId;

            $productResource->weight = $product["weight"];

            $productResource->length = $product["length"];
            $productResource->width = $product["width"];
            $productResource->height = $product["height"];


            $productResource->update(true);
//               
        }
    }

    public function insertProductCategoryRelation($debug = false) {
        $request = \resources\Request::create();
        $categoryResource = \resources\Category::create();
        foreach ($this->products as $product) {
            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=" . $productId);

            foreach ($product["categories"] as $productCategory) {

                $categorySRId = $categoryResource->createSRId($productCategory);

                $productCategoryRelationResource = resources\ProductCategoryRelation::create();
                $productCategoryRelationResource->product = array('id' => $productSRId);
                $productCategoryRelationResource->category = array('id' => $categorySRId);

                $productCategoryRelationResource->setPathInsert();
                $request->addBatch($productCategoryRelationResource->createBatchArray());
            }
        }
        $request->run($debug);
    }

    public function updateProductPrice() {
        $request = \resources\Request::create();
        foreach ($this->products as $product) {
            $productResource = \resources\Product::create();
            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=" . $productId);
            $productResource->id = $productSRId;

            $query = "SELECT * FROM gomb_hikashop_price WHERE price_access = 'all' AND price_product_id = $productId";
            $result = $this->db->query($query);
            $price = 0;
            if (count($result) > 0) {
                $price = $result[0]["price_value"];
            } else {
                $query = "SELECT * FROM gomb_hikashop_price WHERE price_product_id = $productId";
                $res = $this->db->query($query);
                if (count($res) > 0) {
                    $price = $res[0]["price_value"];
                }
            }

            $productResource->price = $price;
            $productResource->setPathUpdate();
            $request->addBatch($productResource->createBatchArray());
        }
        $request->run(true);
    }

    public function insertProductDescription($debug = false) {
        $request = resources\Request::create();
        foreach ($this->products as $product) {
            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=" . $productId);

            $productDescritionResource = resources\ProductDescription::create();

            $productDescritionResource->name = $product["name"];
            $productDescritionResource->description = $product["description"];
//            $productDescritionResource->shortDescription = $product["short_description"];
            $productDescritionResource->metaDescription = $product["meta_description"];
            $productDescritionResource->metaKeywords = $product["meta_keyword"];


            $productDescritionResource->language = array("id" => LANG_HU);
            $productDescritionResource->product = array("id" => $productSRId);


            $productDescritionResource->setPathInsert();
            $request->addBatch($productDescritionResource->createBatchArray());
        }

        $request->run($debug);
    }

    public function getAttributValue($attributeName, $product, $tableName) {
        $result = "";

        if (key_exists($attributeName, $product)) {
            $result = $product[$attributeName];
        } else {
            $productSku = substr($product["StockCode"], 1);
            $query = "SELECT $attributeName FROM $tableName WHERE cikkszam = '$productSku'";
            if ($this->db->isExist($query)) {
                $result = $this->db->query($query)[0][$attributeName];
            }
        }

        return $result;
    }

    public function updateProductCategory() {
        foreach ($this->products as $product) {
            $productId = $product["id_prod"];
            $productSRId = base64_encode("product-product_id=" . $productId);

            $query = "SELECT * FROM products_categ WHERE id_prod = " . $product["id_prod"];
            $categories = $this->db->query($query);
            foreach ($categories as $cat) {
                $categoryId = $cat["id_categ"];

                $categorySRId = base64_encode("category-category_id=" . $categoryId);
                $productCategoryRelationResource = resources\ProductCategoryRelation::create();
                $productCategoryRelationResource->product = array('id' => $productSRId);
                $productCategoryRelationResource->category = array('id' => $categorySRId);

//                sout($productCategoryRelationResource->getData());
                $productCategoryRelationResource->insert(true);
                $this->request->addBatch($productCategoryRelationResource->createBatchArray());
            }
        }
    }

    public function insertProductImage($debug = false) {
        $request = \resources\Request::create();

        foreach ($this->products as $product) {

            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=" . $productId);

            foreach ($product["images"] as $image) {

                $productImageResource = resources\ProductImage::create();
                $productImageResource->imagePath = $image["imagePath"];
//                $productImageResource->imageAlt = $image["imageAlt"];
                $productImageResource->product = array("id" => $productSRId);

//                    $productImageResource->insert(true);
                $productImageResource->setPathInsert();
                $request->addBatch($productImageResource->createBatchArray());
            }
        }

        $request->run($debug);
    }

    public function insertProductsSpecial() {

        $request = \resources\Request::create();
        foreach ($this->products as $product) {
            $productId = $product["shop_products_id"];
            $productSRId = base64_encode("product-product_id=" . $productId);



            if ($product["shop_products_price_action"] != "") {
                $productProductBadgeRelationResource = \resources\ProductProductBadgeRelation::create();
                $productProductBadgeRelationResource->product = array("id" => $productSRId);
                $productProductBadgeRelationResource->productBadge = array("id" => PRODUCT_BADGE);
                $productProductBadgeRelationResource->language = array("id" => LANG_HU);

//                $productProductBadgeRelationResource->insert(true);
                $request->addBatch($productProductBadgeRelationResource->createBatchArray());

                $productSpecialResource = \resources\ProductSpecial::create();

//                $productSpecialResource->priority = $specialValue["priority"];
                $productSpecialResource->price = getNetPrice($product["shop_products_price_action"], 27);
//                $productSpecialResource->dateFrom = $specialValue["date_start"];
//                $productSpecialResource->dateTo = $specialValue["date_end"];
//                $customer_group_id = $specialValue["customer_group_id"];
//                $customerGroupSRID = $customerGroupResource->switchId($customer_group_id);
//                $productSpecialResource->customerGroup = array("id" => $customerGroupSRID);

                $productSpecialResource->product = array("id" => $productSRId);
//                    $productSpecialResource->insert(true);
                $request->addBatch($productSpecialResource->createBatchArray());
            }
        }

        $request->run();
    }

    public function addBadge($productSRId) {
        $productProductBadgeRelatinResource = \resources\ProductProductBadgeRelation::create();
        $productProductBadgeRelatinResource->product = array("id" => $productSRId);
        $productProductBadgeRelatinResource->productBadge = array("id" => PRODUCT_BADGE);
        $productProductBadgeRelatinResource->language = array("id" => LANG_HU);
        $productProductBadgeRelatinResource->insert();
    }

    public function createProductClass() {
        $query = "SELECT * FROM some_codes";
        $parentProducts = $this->db->query($query);

        foreach ($parentProducts as $parentProductValue) {


            $productId = intval($parentProductValue["id"]);
            $productId = PARENT_ID_NUMBER_ADD + $productId;
            $productSRId = base64_encode("product-product_id=" . $productId);

            $productClassResource = \resources\ProductClass::create();
            $productClassResource->name = $parentProductValue["title"] . " - " . $parentProductValue["id"];
            $productClassResource->description = "";


            $productClassResult = $productClassResource->insert(true);

            $productResource = resources\Product::create();

            $sku = "productGroup-" . $productId;
            $productResource->sku = $sku;
            $productResource->productClass = array("id" => $productClassResult["id"]);

//            $productSRId = $productResource->getProductIdBySku($sku);
            $productResource->id = $productSRId;
            $productResource->update(true);

            $productResource = resources\Product::create();
            $productResource->productClass = array("id" => $productClassResult["id"]);
            $productResource->parentProduct = array("id" => $productSRId);


            foreach ($this->products as $child) {
                if ($child["some"] == $parentProductValue["id"]) {
                    $childSRId = $productResource->createSRId($child["id_prod"]);
                    $sku = $child["kod"] . "-" . $child["id_prod"];
                    if ($child["kod"] == "") {
                        $sku = $child["id_prod"] . "-" . $child["id_prod"];
                    }
                    $productResource->sku = $sku;
                    $productResource->id = $childSRId;
                    $productResource->update(true);
                }
            }
        }
    }

    public function createProductAttribute($attributeHelper) {

        $attributes = $attributeHelper->getAttributes();

        for ($i = 0; $i < count($attributes); $i++) {
            $attribute = $attributes[$i];

            $listAttributeResource = resources\ListAttribute::create();
            $listAttributeResource->type = "LIST";
            $listAttributeResource->name = $attribute->getCimke();
            $listAttributeResource->priority = "NORMAL";
            $listAttributeResource->sortOrder = "NORMAL";
            $listAttributeResource->required = 0;
            $listAttributeResource->presentation = "TEXT";

            $listAttributeResult = $listAttributeResource->insert(true);

            $attributeDescriptionResource = \resources\AttributeDescription::create();


            $attributeDescriptionResource->name = $attribute->getNev();
            $attributeDescriptionResource->description = "";
            $attributeDescriptionResource->attribute = array('id' => $listAttributeResult["id"]);
            $attributeDescriptionResource->language = array('id' => LANG_HU);
            $attributeDescriptionResource->insert(true);

            foreach ($this->productClass as $productClass) {
                $productClassAttributeRelationResource = \resources\ProductClassAttributeRelation::create();
                $productClassAttributeRelationResource->productClass = array('id' => $productClass["id"]);
                $productClassAttributeRelationResource->attribute = array('id' => $listAttributeResult["id"]);
                $productClassAttributeRelationResource->insert(true);
            }


            foreach ($attributeHelper->getProductClassArray() as $productClass) {

                if (array_key_exists("first", $productClass)) {
                    if ($attribute->getCimke() == $productClass["first"]) {
                        $productClassResource = \resources\ProductClass::create();
                        $productClassResource->id = $productClass["id"];
                        $productClassResource->firstVariantSelectType = "SELECT";
                        $productClassResource->firstVariantParameter = array("id" => $listAttributeResult["id"]);
                        $productClassResource->update(true);
                    }
                }
                if (array_key_exists("second", $productClass)) {
                    if ($attribute->getCimke() == $productClass["second"]) {
                        $productClassResource = \resources\ProductClass::create();
                        $productClassResource->id = $productClass["id"];
                        $productClassResource->secondVariantSelectType = "SELECT";
                        $productClassResource->secondVariantParameter = array("id" => $listAttributeResult["id"]);
                        $productClassResource->update(true);
                    }
                }
            }

            foreach ($attribute->getAttributeValues() as $attributeValue) {
                $name = $attributeValue->name;

                $listAttributeValueResource = \resources\ListAttributeValue::create();
                $listAttributeValueResource->listAttribute = array('id' => $listAttributeResult['id']);

                $resultAV = $listAttributeValueResource->insert(true);

                $listAttributeValueDescriptionResource = \resources\ListAttributeValueDescription::create();
                $listAttributeValueDescriptionResource->listAttributeValue = array('id' => $resultAV["id"]);
                $listAttributeValueDescriptionResource->language = array('id' => LANG_HU);
                $listAttributeValueDescriptionResource->name = $name;

                $resultAVD = $listAttributeValueDescriptionResource->insert(true);

                foreach ($attributeValue->productIdArray as $childId) {
                    $productSRId = base64_encode("product-product_id=" . $childId);
                    $productListAttributeValueRelationResource = \resources\ProductListAttributeValueRelation::create();
                    $productListAttributeValueRelationResource->product = array("id" => $productSRId);
                    $productListAttributeValueRelationResource->listAttributeValue = array("id" => $resultAV["id"]);
                    sout($productListAttributeValueRelationResource->getData());
                    $productClassAttributeRelationsResult = $productListAttributeValueRelationResource->insert(true);
                }
            }
        }
    }

    public function insertProductTag($debug = false) {

        $request = \resources\Request::create();
        foreach ($this->products as $product) {

            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=" . $productId);


            if ($product["tag"] != "") {
                $productTag = \resources\ProductTag::create();
                $productTag->product = array("id" => $productSRId);
                $productTag->language = array("id" => LANG_HU);
                $productTag->tags = $product["tag"];
                $productTag->setPathInsert();
                $request->addBatch($productTag->createBatchArray());
            }
        }
//        sout($request);
        $request->run($debug);
    }

    public function insertOneProductOption($debug = false) {
        foreach ($this->products as $product) {
            if (trim($product["size"]) != "") {

                $productId = $product[PRODUCT_ID];
                $productSRId = base64_encode("product-product_id=" . $productId);

                $productOptionResource = resources\ProductOption::create();
                $productOptionResource->product = array("id" => $productSRId);

                $resultProductOptionResource = $productOptionResource->insert(true);

                $productOptionDescriptionResource = resources\ProductOptionDescription::create();
                $productOptionDescriptionResource->name = "Méret";
                $productOptionDescriptionResource->productOption = array("id" => $resultProductOptionResource["id"]);
                $productOptionDescriptionResource->language = array("id" => LANG_HU);

                $resultProductOptionDescriptionResource = $productOptionDescriptionResource->insert(true);


                $productOptionValueResource = \resources\ProductOptionValue::create();
                $productOptionValueResource->prefix = "+";
                $productOptionValueResource->productOption = array("id" => $resultProductOptionResource["id"]);
                $productOptionValueResource->price = 0;


                $resultProductOptionValueResource = $productOptionValueResource->insert(true);

                $productOptionValueDescriptionResource = resources\ProductOptionValueDescription::create();
                $productOptionValueDescriptionResource->name = $product["size"];
                $productOptionValueDescriptionResource->productOptionValue = array("id" => $resultProductOptionValueResource["id"]);
                $productOptionValueDescriptionResource->language = array("id" => LANG_HU);

                $productOptionValueDescriptionResource->insert(true);
            }
        }
    }

    public function insertProductOption($debug = false) {

        $request = \resources\Request::create();
        $innerId = 1;
        $valueInnerId = 1;
        foreach ($this->products as $product) {
            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=" . $productId);


            foreach ($product["options"] as $option) {
                $productOptionResource = resources\ProductOption::create();
                $optionName = $option["name"];
                $optionLabel = $option["label"];

                $productOptionResourceSRId = slug("prod_option_" . $optionName . "_" . $productId);
                $productOptionResource->id = $productOptionResourceSRId;
                $productOptionResource->product = array("id" => $productSRId);

                $productOptionResource->setPathUpdate();
                $request->addBatch($productOptionResource->createBatchArray());


                $productOptionDescriptionResource = resources\ProductOptionDescription::create();
//                $productOptionDescriptionResource->id = ("prod_option_desc_" . $optionName . "_" . $productId);
                $productOptionDescriptionResource->name = $optionLabel;
                $productOptionDescriptionResource->productOption = array("id" => $productOptionResourceSRId);
                $productOptionDescriptionResource->language = array("id" => LANG_HU);

                $productOptionDescriptionResource->setPathInsert();
                $request->addBatch($productOptionDescriptionResource->createBatchArray());


                $index = 1;
                foreach ($option["values"] as $optionValue) {

                    $productOptionValueResource = \resources\ProductOptionValue::create();
                    $productOptionValueResourceSRId = "prod_option_val_" . $optionName . "_" . $productId . "_" . $index;
                    $productOptionValueResource->id = $productOptionValueResourceSRId;
                    $productOptionValueResource->prefix = "+";
                    $productOptionValueResource->productOption = array("id" => $productOptionResourceSRId);
                    $productOptionValueResource->price = $optionValue["price"];

                    $productOptionValueResource->setPathUpdate();
                    $request->addBatch($productOptionValueResource->createBatchArray());
//                    $resultProductOptionValueResource = $productOptionValueResource->insert(true);

                    $productOptionValueDescriptionResource = resources\ProductOptionValueDescription::create();
//                    $productOptionValueDescriptionResource->id = "prod_option_val_desc_" . $optionName . "_" . $productId . "_" . $index;
                    $productOptionValueDescriptionResource->name = $optionValue["name"];
                    $productOptionValueDescriptionResource->productOptionValue = array("id" => $productOptionValueResourceSRId);
                    $productOptionValueDescriptionResource->language = array("id" => LANG_HU);

                    $productOptionValueDescriptionResource->setPathInsert();
                    $request->addBatch($productOptionValueDescriptionResource->createBatchArray());

                    $index++;
//                    $productOptionValueDescriptionResource->insert(true);
                }
            }
        }
        $request->run($debug);
    }

//kapcsolódó 
    public function insertRelatedProductRelations($debug = false) {
        $request = \resources\Request::create();

        foreach ($this->products as $product) {
            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=" . $productId);

            foreach ($product["relatedProducts"] as $relatedProductId) {
                $productRelatedProductRelation = resources\ProductRelatedProductRelation::create();

                $relatedProductSRId = base64_encode("product-product_id=" . $relatedProductId);

                $productRelatedProductRelation->product = array("id" => $productSRId);
                $productRelatedProductRelation->relatedProduct = array("id" => $relatedProductSRId);

                $productRelatedProductRelation->setPathInsert();
                $request->addBatch($productRelatedProductRelation->createBatchArray());
            }
        }

        $request->run($debug);
    }

//kiegészítő
    public function insertCollateralProductRelation() {
        $request = \resources\Request::create();
        foreach ($this->products as $product) {
            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=" . $productId);
            foreach ($product["collateralProducts"] as $collateralProductId) {
                $collateralProductSRId = base64_encode("product-product_id=" . $collateralProductId);
                $productCollateralProductRelation = resources\ProductCollateralProductRelation::create();
                $productCollateralProductRelation->product = array("id" => $productSRId);
                $productCollateralProductRelation->collateralProduct = array("id" => $collateralProductSRId);

                $productCollateralProductRelation->setPathInsert();
                $request->addBatch($productCollateralProductRelation->createBatchArray());
            }
        }
        $request->run(true);
    }

    public function insertProductUrlAlias($debug = false) {
        $request = \resources\Request::create();
        foreach ($this->products as $product) {
            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=" . $productId);


            $url = slug($product["name"] . "-" . $productId);

            $urlAlias = \resources\URLAlias::create();
            $urlAlias->type = "PRODUCT";
            $urlAlias->urlAliasEntity = array("id" => $productSRId);
            $urlAlias->urlAlias = $url;

            $urlAlias->setPathInsert();
            $request->addBatch($urlAlias->createBatchArray());
//            $urlAlias->insert(true);
        }

        $request->run($debug);
    }

    public function insertCategoryUrlAlias() {
        $urlAliasResource = new \resources\URLAlias();
        $urlAliasResource->deleteCategoryUrl();

        $query = "SELECT * FROM ps_category";
        $categories = $this->db->query($query);
        foreach ($categories as $category) {

            $categoryId = $category["id_category"];
            $categorySRId = base64_encode("category-category_id=" . $categoryId);

            $query = "SELECT link_rewrite FROM ps_category_lang WHERE id_lang = 6 AND id_category = " . $categoryId;
            $link_rewrite = $this->db->findOne($query, "link_rewrite");
            $urlAlias = $categoryId . "-" . $link_rewrite;

            $urlAliasResource->setType("CATEGORY");
            $urlAliasResource->setUrlAliasEntity(["id" => $categorySRId]);
            $urlAliasResource->setUrlAlias($urlAlias);
            $urlAliasResource->insert();
        }
    }

    public function insertProductSpecial($debug = false) {
        $request = \resources\Request::create();
        $customerGroupResource = \resources\CustomerGroup::create();
        foreach ($this->products as $product) {

            foreach ($product["special"] as $special) {


                $productId = $product[PRODUCT_ID];
                $productSRId = base64_encode("product-product_id=" . $productId);

                $price = $special["price"];

                $productSpecialResource = \resources\ProductSpecial::create();
                $productSpecialResource->price = $price;
                $productSpecialResource->product = array("id" => $productSRId);

                if ($special[CUSTOMER_GROUP_ID] != 0) {
                    $productSpecialResource->customerGroup = array("id" => $customerGroupResource->createOuterId($special[CUSTOMER_GROUP_ID]));
                }
//
                $productSpecialResource->dateFrom = $special["date_start"];
                $productSpecialResource->dateTo = $special["date_end"];
//                $productSpecialResource->minQuantity = $special["from_quantity"];
                $productSpecialResource->setPathInsert();
                $request->addBatch($productSpecialResource->createBatchArray());

                $productProductBadgeRelationResource = \resources\ProductProductBadgeRelation::create();
                $productProductBadgeRelationResource->product = array("id" => $productSRId);
                $productProductBadgeRelationResource->productBadge = array("id" => PRODUCT_BADGE);
                $productProductBadgeRelationResource->language = array("id" => LANG_HU);
//
                $productProductBadgeRelationResource->setPathInsert();
                $request->addBatch($productProductBadgeRelationResource->createBatchArray());
            }
        }
        $request->run($debug);
    }

    public function insertCustomerGroupProductPrice() {

        $request = \resources\Request::create();
        foreach ($this->products as $product) {
            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=" . $productId);

            $query = "SELECT * FROM gomb_hikashop_price WHERE price_min_quantity = 0 AND price_product_id = $productId";
            $specialProducts = $this->db->query($query);

            foreach ($specialProducts as $specProduct) {

                $price = $specProduct["price_value"];

                $customerGroupProductPriceResource = \resources\CustomerGroupProductPrice::create();

                $customerGroupIdString = $specProduct["price_access"];
                if ($customerGroupIdString != 'all' && $customerGroupIdString != 'none') {

                    $result = explode(",", $specProduct["price_access"]);
                    foreach ($result as $value) {
                        if (is_numeric($value)) {
                            $customerGroupSRID = "customer_group_" . $value;
                            $customerGroupProductPriceResource->customerGroup = array("id" => $customerGroupSRID);
                            $customerGroupProductPriceResource->product = array("id" => $productSRId);
                            $customerGroupProductPriceResource->price = $price;

//                            sout($customerGroupProductPriceResource->getData());
                            $customerGroupProductPriceResource->setPathInsert();
                            $request->addBatch($customerGroupProductPriceResource->createBatchArray());
                        }
                    }
                }
            }
        }

        $request->run(true);
    }

    public function insertListAttribute($debug = false) {

        $this->db->query("SET @cnt = 0");
        $listAttributes = $this->db->query(LIST_ATTRIBUTE_QUERY);

        $request = \resources\Request::create();

        $sortOder = 1;
        foreach ($listAttributes as $listAttribute) {
            $listAttributeId = $listAttribute[LIST_ATTRIBUTE_ID];
            $listAttributeSRId = "listAttribute_$listAttributeId";

            $listAttributeResource = resources\ListAttribute::create();
            $listAttributeResource->id = $listAttributeSRId;
            $listAttributeResource->type = "LIST";
            $listAttributeResource->name = $listAttributeSRId;
            $listAttributeResource->priority = "NORMAL";
            $listAttributeResource->sortOrder = $sortOder++;
            $listAttributeResource->required = 0;
            $listAttributeResource->presentation = "TEXT";

            $listAttributeResource->setPathUpdate();
//            $listAttributeResource->setMethod("DELETE");
            $request->addBatch($listAttributeResource->createBatchArray());
//            $listAttributeResource->insertWithOuterId(true);
        }

        $request->run($debug);
    }

    public function insertAttributeDescription($debug = false) {

        $listAttributes = $this->db->query(LIST_ATTRIBUTE_QUERY);

        $request = \resources\Request::create();
        foreach ($listAttributes as $listAttribute) {
            $listAttributeId = $listAttribute[LIST_ATTRIBUTE_ID];
            $listAttributeSRId = "listAttribute_$listAttributeId";

            $attributeDescriptionResource = resources\AttributeDescription::create();
//            $attributeDescriptionResource->id = "attributeDescription_$listAttribute";
            $attributeDescriptionResource->name = $listAttribute["name"];
            $attributeDescriptionResource->description = "";
            $attributeDescriptionResource->attribute = array('id' => $listAttributeSRId);
            $attributeDescriptionResource->language = array('id' => LANG_HU);

            $attributeDescriptionResource->setPathInsert();
            $request->addBatch($attributeDescriptionResource->createBatchArray());
        }

        $request->run($debug);
    }

    public function insertProductClassAttributeRelation($debug = false) {
        $listAttributes = $this->db->query(LIST_ATTRIBUTE_QUERY);

        $request = resources\Request::create();

        foreach ($listAttributes as $listAttribute) {
            $listAttributeId = $listAttribute[LIST_ATTRIBUTE_ID];
            $listAttributeSRId = "listAttribute_$listAttributeId";

//            $productClassArray = $this->db->query(PRODUCT_CLASS_QUERY);
//            foreach ($productClassArray as $productClassValue) {
//            $productClassId = $productClassValue[PRODUCT_CLASS_ID];
//            $productClassSRId = slug($productClassValue["name"]) . "_" . $productClassId;
//            $productClass = resources\ProductClass::create();
//            $productClass->id = $productClassSRId;

            $productClassAttributeRelationResource = resources\ProductClassAttributeRelation::create();
//                    $productClassAttributeRelationResource->id = "productClassAttributeRelation_" . $categoryId . "_" . $counter++;
            $productClassAttributeRelationResource->productClass = array('id' => SR_DEFAULT_PRODUCT_CLASS);
            $productClassAttributeRelationResource->attribute = array('id' => $listAttributeSRId);

            $productClassAttributeRelationResource->setPathInsert();

            $request->addBatch($productClassAttributeRelationResource->createBatchArray());
//            }
        }
        $request->run($debug);
    }

    public function insertListAttributeValue($debug = false, $method = "POST") {

        $request = \resources\Request::create();


        foreach ($this->products as $product) {
            $productId = $product[PRODUCT_ID];
            $productSRId = base64_encode("product-product_id=$productId");

            foreach ($product["listAttributes"] AS $listAttribute) {


                $listAttributeSRId = $listAttribute["sr_id"];
                $listAttributeValueResourceSRId = "listAttributeValue_$productId-listAttribute_$listAttributeSRId";

                $listAttributeValueResource = resources\ListAttributeValue::create();
                $listAttributeValueResource->id = $listAttributeValueResourceSRId;
                $listAttributeValueResource->listAttribute = array('id' => $listAttributeSRId);

                $listAttributeValueResource->setPathUpdate();
                $listAttributeValueResource->setMethod($method);
                $request->addBatch($listAttributeValueResource->createBatchArray());

                $listAttributeValueDescriptionResource = \resources\ListAttributeValueDescription::create();
                $listAttributeValueDescriptionResourceSRId = "listAttributeValueDescription_$productId-listAttribute_$listAttributeSRId";
                $listAttributeValueDescriptionResource->id = $listAttributeValueDescriptionResourceSRId;
                $listAttributeValueDescriptionResource->listAttributeValue = array('id' => $listAttributeValueResourceSRId);
                $listAttributeValueDescriptionResource->language = array('id' => LANG_HU);
                $listAttributeValueDescriptionResource->name = $listAttribute["value"];


                $listAttributeValueDescriptionResource->setPathUpdate();
                $listAttributeValueDescriptionResource->setMethod($method);
                $request->addBatch($listAttributeValueDescriptionResource->createBatchArray());

                $productListAttributeValueRelationResource = \resources\ProductListAttributeValueRelation::create();
                $productListAttributeValueRelationResourceSRId = "productListAttributeValueRelation_$productId-listAttribute_$listAttributeSRId";
                $productListAttributeValueRelationResource->id = $productListAttributeValueRelationResourceSRId;
                $productListAttributeValueRelationResource->product = array("id" => $productSRId);
                $productListAttributeValueRelationResource->listAttributeValue = array("id" => $listAttributeValueResourceSRId);

                $productListAttributeValueRelationResource->setPathUpdate();
                $productListAttributeValueRelationResource->setMethod($method);
                $request->addBatch($productListAttributeValueRelationResource->createBatchArray());

//                sout("ADD to request: ");
//                sout($productListAttributeValueRelationResourceSRId);
            }
        }
        $request->run($debug);
    }

    public function setProductClassFirstAndSecondVariantParameter() {
        $query = "SELECT product_parent_id FROM gomb_hikashop_product WHERE product_parent_id != 0 GROUP BY product_parent_id";

        $result = $this->db->query($query);

        $params = array();
        foreach ($result as $value) {
            $param = Param::create();
            $param->setProductClassId($value["product_parent_id"]);

            $query = "SELECT * FROM gomb_hikashop_variant WHERE variant_product_id = " . $param->getProductClassId();
            $res = $this->db->query($query);


            if (count($res) > 0) {
                foreach ($res as $value) {
                    $cId = $value["variant_characteristic_id"];
                    $param->add($cId);
                }
                $params[] = $param;
            }
        }



        $this->productClassFSParams = $params;
        sout($this->productClassFSParams);
    }

    public function updateProductClass($debug = false) {

        $request = resources\Request::create();
        $productClassArray = $this->db->query(PRODUCT_CLASS_QUERY);


        foreach ($productClassArray as $productClassValue) {
            $productId = $productClassValue[PRODUCT_ID];
            $productClassSRId = slug($productClassValue["name"]) . "_$productId";
            $productClassResource = resources\ProductClass::create();
            $productClassResource->id = $productClassSRId;

            $productClassResource->firstVariantSelectType = "SELECT";
            $productClassResource->firstVariantParameter = array("id" => "szin");

            $productClassResource->secondVariantSelectType = "SELECT";
            $productClassResource->secondVariantParameter = array("id" => "meret");

            $productClassResource->setPathUpdate();
            $request->addBatch($productClassResource->createBatchArray());
        }


        $request->run($debug);
    }

    public function updateProductsStatus() {

        foreach ($this->products as $product) {
            $productResource = \resources\Product::create();
            $productId = $product["ID"];
            $productSRId = base64_encode("product-product_id=" . $productId);

            $productResource->id = $productSRId;


            $status = 1;
            if ($product["post_status"] == "draft") {
                $status = 0;
            }

            $productResource->status = $status;

//            sout($productResource->getData());
            $productResource->update(true);
        }
    }

    public function updateProductSku() {
        foreach ($this->products as $product) {
            $productResource = \resources\Product::create();
            $productId = $product["id_prod"];
            $productSRId = base64_encode("product-product_id=" . $productId);

            $productResource->id = $productSRId;
            $sku = $productResource->sku;

            $arr = explode("-", $sku);

            if ($arr[0] != 'productGroup') {
                $newSku = $arr[0];
                $productResource->sku = $newSku;
                sout($productResource->getData());
//                    $productResource->update(true);
            }
        }
    }

    public function updateProductsManufacturer() {

        $query = "SELECT * FROM wp_terms WHERE term_id IN(SELECT term_id FROM `wp_term_taxonomy` WHERE taxonomy = 'pa_marka')";
        $manufacturers = $this->db->query($query);

        $manufacturerArray = array();
        foreach ($manufacturers as $manufacturer) {
            $query = "SELECT * FROM wp_term_taxonomy WHERE term_id = " . $manufacturer["term_id"];
            $res = $this->db->query($query);
            $taxonomy = $res[0];
            $query = "SELECT * FROM wp_term_relationships WHERE term_taxonomy_id = " . $taxonomy["term_taxonomy_id"];
            $objects = $this->db->query($query);
            $productIDs = array();
            foreach ($objects as $object) {
                $productIDs[] = $object["object_id"];
            }
            $manu = array();
            $manu["manufacturer_id"] = $manufacturer["term_id"];
            $manu["product_id_array"] = $productIDs;
            $manufacturerArray[] = $manu;
        }



        foreach ($manufacturerArray as $manu) {
            $manufacturerId = $manu["manufacturer_id"];
            $manufacturerSRId = base64_encode("manufacturer-manufacturer_id=" . $manufacturerId);

            foreach ($manu["product_id_array"] as $productId) {
                $productResource = \resources\Product::create();
                $productSRId = $productResource->createSRId($productId);
                $productResource->id = $productSRId;
                $productResource->manufacturer = array("id" => $manufacturerSRId);
//            sout($productResource->getData());
                $productResource->update(true);
            }
        }
    }

    public function create301Product() {
        $csvArray = array();
        foreach ($this->products as $product) {
            $productId = $product["id_product"];
            $url = $productId . "-" . $product["link_rewrite"];
            $csv["old_url"] = $url . ".html";
            $csv["new_url"] = $url;
            $csv["order"] = 0;
            $csvArray[] = $csv;
//            foreach ($this->childProducts as $child) {
//                if ($child["parent_id"] == $productId) {
//                    $parentId = $child["parent_id"];
//                    $urlChild = $parentId . "-" . $child["increment"] . "-" . $product["link_rewrite"];
//                    $csvArray["old_url"] = $urlChild . ".html";
//                    $csvArray["new_url"] = $urlChild;
//                }
//            }
        }
        $headers = ['old_url', 'new_url', 'order'];
        $fp = fopen('data/sutnifozniProductURL.csv', 'w');
        fputcsv($fp, $headers, ";");
        foreach ($csvArray as $fields) {
            fputcsv($fp, array_values($fields), ";");
        }
        fclose($fp);
    }

    private function createImageUrlFromImageId($imgId) {
        $array = str_split($imgId);
        $dir = "p/";
        foreach ($array as $value) {
            $dir .= $value . "/";
        }

        $url = $dir . $imgId . "-watermark.jpg";

        return $url;
    }

    private function removoIfMatch($string, $character) {
        if (substr($string, -1) === $character) {
            return substr($string, 0, -1);
        } else {
            return $string;
        }
    }

    public function deleteProduct($list) {
        $deleteResource = \helpers\DeleteResource::create();

        $deleteResource->setList($list);
        $deleteResource->setIsDelete(true);
        $deleteResource->deleteList();
    }

    public function isChild($productId) {
        $query = "SELECT * FROM gomb_hikashop_product WHERE product_id = $productId";
        $result = $this->db->query($query);

        if (count($result) > 0) {
            $product = $result[0];
            if ($product[PRODUCT_PARENT_ID] != "0") {
                return true;
            }
        } else {
            return false;
        }
    }

    public function isParent($productId) {
        $query = "SELECT * FROM gomb_hikashop_product WHERE product_parent_id = $productId";
        $result = $this->db->query($query);

        if (count($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getImageAsArray($image) {
        $result = array();
        if ($image != "") {
            $array = explode("\"", $image);

            foreach ($array as $value) {
                if ($value[0] == ".") {
                    $img = "";
                    $img = str_replace("\\", "", $value);
                    $img = str_replace("../", "", $img);
                    $result[] = $img . ".jpg";
                }
            }
        }

        return $result;
    }

    public function createProductOptionArray($productId) {
        $query = "SELECT * FROM webshop_products WHERE prod_id = $productId";
        $result = $this->db->query($query);
        $product = $result[0];
        $options = array();

        if ($product["swatches"] != "") {
            $swatchOption = Option::create();
            $swatchOption->name = "Szövetminták";

            $optionValueArray = array();
            $array = explode(",", $product["swatches"]);


            foreach ($array as $value) {

                $query = "SELECT * FROM product_swatches WHERE id = $value AND deleted = 0";
                if ($this->db->isExist($query)) {
                    $swatch = $this->db->query($query);
                    $swatchVal = $swatch[0];

                    $optionValue = Value::create();
                    $optionValue->name = $swatchVal["name"];
                    $optionValueArray[] = $optionValue;
                }
            }

            $swatchOption->values = $optionValueArray;

            $options[] = $swatchOption;
        }


        $query = "SELECT * FROM webshop_products_details WHERE  prod_id = $productId AND deleted = 0 GROUP BY name ";
        if ($this->db->isExist($query)) {

            $detailArray = $this->db->query($query);
            foreach ($detailArray as $detailValue) {
                $choice_id = $detailValue["choice_id"];
                $name = $detailValue["name"];

                $optionDetail = Option::create();
                $optionDetail->name = $name;
                $query = "SELECT * FROM webshop_products_details WHERE prod_id = $productId AND name = '$name' AND deleted = 0";


                $paramArray = $this->db->query($query);
                $optionValueArray = array();
                foreach ($paramArray as $paramValue) {
                    $optionValue = Value::create();
                    $optionValue->name = $paramValue["choice"];
                    if ($paramValue["price"] != 0) {
                        $optionValue->prefix = "+";
                        $optionValue->price = $paramValue["price"];
                    }
                    $optionValueArray[] = $optionValue;
                }

                $optionDetail->values = $optionValueArray;

                $options[] = $optionDetail;
            }
        }


        if ($product["transport_price"] != 0) {
            $transportOption = Option::create();
            $transportOption->name = "Házhozszállítást kér?";

            $optionValueArray = array();

            $optionValue = Value::create();
            $optionValue->name = "Igen";
            $optionValue->prefix = "+";
            $optionValue->price = $product["transport_price"];
            $optionValueArray[] = $optionValue;

            $optionValue = Value::create();
            $optionValue->name = "Nem";
            $optionValueArray[] = $optionValue;

            $transportOption->values = $optionValueArray;

            $options[] = $transportOption;
        }

        return $options;
    }

}

class Param {

    protected $productClassId;
    protected $first;
    protected $second;
    protected $db;

    private function __construct() {
        $this->db = \db\Database::instance();
    }

    public static function create() {
        $p = new Param();
        return $p;
    }

    function getProductClassId() {
        return $this->productClassId;
    }

    function getFirst() {
        return $this->first;
    }

    function getSecond() {
        return $this->second;
    }

    function setProductClassId($productClassId) {
        $this->productClassId = $productClassId;
    }

    function setFirst($first) {
        $this->first = $first;
    }

    function setSecond($second) {
        $this->second = $second;
    }

    function add($param) {
        if ($this->isParent($param)) {
            if ($this->getFirst() == NULL) {
                $this->setFirst($param);
            } elseif ($this->getFirst() != NULL AND $this->getSecond() == NULL) {
                $this->setSecond($param);
            } else {
//            sout($param);
            }
        }
    }

    public function isParent($characteristicId) {
        $query = "SELECT * FROM gomb_hikashop_characteristic WHERE characteristic_id = $characteristicId";
        $result = $this->db->query($query);

        if (array_key_exists(0, $result)) {
            if ($result[0]["characteristic_parent_id"] == "0") {
                return true;
            }
        }

        return false;
    }

}

class Option {

    public $name;
    public $values;

    protected function __construct() {
        ;
    }

    public static function create() {
        return new Option();
    }

}

class Value {

    public $prefix;
    public $price;
    public $name;

    protected function __construct() {
        ;
    }

    public static function create() {
        return new Value();
    }

}
