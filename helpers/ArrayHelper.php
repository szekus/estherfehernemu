<?php

namespace helpers;

class ArrayHelper {

    protected $array;

    public static function create() {
        $arrayHelper = new ArrayHelper();
        return $arrayHelper;
    }

    protected function __construct() {
        ;
    }

    public function getValueByKey($key, $defaultValue = "") {
        foreach ($this->array as $arr) {
            if ($arr["meta_key"] == $key) {
                return $arr["meta_value"];
            }
        }
        return $defaultValue;
    }

    function getArray() {
        return $this->array;
    }

    function setArray($array) {
        $this->array = $array;
    }

}
