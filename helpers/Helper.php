<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace helpers;

/**
 * Description of Helper
 *
 * @author szekus
 */
class Helper {

    protected $db;
    protected $limit = 5;
    protected $counter = 0;
    protected $request;
    protected $mainQuery;

    
    function getMainQuery() {
        return $this->mainQuery;
    }

    function setMainQuery($mainQuery) {
        $this->mainQuery = $mainQuery;
    }

        function getRequest() {
        return $this->request;
    }

    function setRequest($request) {
        $this->request = $request;
    }

    protected function __construct() {
        $this->db = \db\Database::instance();
        $this->request = \resources\Request::create();
    }

    function getId($url) {
        $arr = explode("/", $url);
        return $arr[count($arr) - 1];
    }

    public static function create() {
        $helper = new Helper();
        return $helper;
    }

}
