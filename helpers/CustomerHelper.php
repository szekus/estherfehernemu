<?php

namespace helpers;

class CustomerHelper extends Helper {

    protected $customers;

    function getCustomers() {
        return $this->customers;
    }

    function setCustomers($debug = false) {
        $this->customers = $this->db->query(CUSTOMER_QUERY);



        foreach ($this->customers as &$customerValue) {
            $customer_id = $customerValue[CUSTOMER_ID];

            $telephone = "(20) 123 4567";
            if ($customerValue["telephone"] == "") {
                $customerValue["telephone"] = $telephone;
            }


            $query = "SELECT * FROM  oc_address WHERE customer_id = $customer_id";
            $addresses = $this->db->query($query);

            foreach ($addresses as &$address) {
                $address["isoCode2"] = "HU";
            }
            $customerValue["addresses"] = $addresses;
        }


        if ($debug) {
            sout($this->customers);
            die();
        }
    }

    protected function __construct() {
        parent::__construct();
    }

    public static function create() {
        $helper = new CustomerHelper();
        return $helper;
    }

    public function updateAllUser() {
        $query = "SELECT email FROM aaasr_orders GROUP BY email";
        $orders = $this->db->query($query);
        foreach ($orders as $order) {
            $email = $order["email"];
            $data = $this->isExistInAllCustomer($email);
            if (!$data) {
//                sout("not regitered in all user table");

                $type = $this->getTypeOfCustomer($email);

                // if new customer, insert into not_registered_customer, and all_customer table
                if ($type == CUSTOMER_NEW) {

                    $query = "INSERT INTO not_registered_customer (id, email) "
                            . "VALUES (NULL, '$email')";

                    $this->db->query($query);

                    $lastId = ($this->db->getGetConnect()->lastInsertId());
                    $em_id = (int) (CUSTOMER_NOT_REGISTERD_ID + (int) $lastId);

                    $query = "UPDATE not_registered_customer 
                              SET em_id = '$em_id'
                              WHERE id = $lastId    
                            ";
                    $this->db->query($query);


                    $query = "INSERT INTO all_customer ( email, em_customer_id,  not_registered_customer) "
                            . "VALUES ('$email', '$em_id', '1')";
                    $this->db->query($query);
                }

                //if customer in SR customer, insert into all_customer table
                if ($type == CUSTOMER_SR) {
                    $query = "SELECT * FROM all_customer WHERE email = '$email'";
                    $result = $this->db->query($query);

                    if (count($result) == 0) {
                        $query = "SELECT * FROM aaasr_customers WHERE email = '$email'";
                        $res = $this->db->query($query);
                        $sr_em_id = (int) (CUSTOMER_SR_ID + (int) $res[0]["innerId"]);
                        $query = "INSERT INTO all_customer ( email, em_customer_id,  sr_customer) "
                                . "VALUES ('$email', '$sr_em_id', '1')";

                        $this->db->query($query);
                    }
                }

                //if customer in scala, insert into all_customer table
                if ($type == CUSTOMER_ISCALA) {
                    $query = "SELECT * FROM all_customer WHERE email = '$email'";
                    $result = $this->db->query($query);

                    if (count($result) == 0) {
                        $query = "SELECT * FROM em_customer_data WHERE email = '$email'";
                        $res = $this->db->query($query);
                        $em_id = $res[0]["em_id"];
                        $query = "INSERT INTO all_customer ( email, em_customer_id,  em_customer) "
                                . "VALUES ('$email', '$sr_em_id', '1')";
                        $this->db->query($query);
                    }
                }
            }
        }

        $query = "SELECT * FROM em_customer_data";
        $result = $this->db->query($query);
        foreach ($result as $EMCustomer) {
            $em_id = $EMCustomer["em_id"];
            $emCustomerEmail = $EMCustomer["email"];
            $query = "INSERT INTO all_customer ( email, em_customer_id,  em_customer) "
                    . "VALUES ('$emCustomerEmail', '$em_id', '1')";

//            sout($query);
            $this->db->query($query);
        }

        $query = "SELECT * FROM aaasr_customers";
        $result = $this->db->query($query);
        foreach ($result as $SRCustomer) {
            $sr_em_id = (int) (CUSTOMER_SR_ID + (int) $SRCustomer["innerId"]);
            $sr_email = $SRCustomer["email"];
            $query = "INSERT INTO all_customer ( email, em_customer_id,  sr_customer) "
                    . "VALUES ('$sr_email', '$sr_em_id', '1')";
//            sout($query);
            $this->db->query($query);
        }
    }

    public function getTypeOfCustomer($email) {
        $result = array();
        $query = "SELECT * FROM em_customer_data WHERE email = '$email'";
        $result = $this->db->query($query);
        if (count($result) > 0) {
            return CUSTOMER_ISCALA;
        }
        $query = "SELECT * FROM aaasr_customers WHERE email = '$email'";
        $result = $this->db->query($query);
        if (count($result) > 0) {
            return CUSTOMER_SR;
        }
        $query = "SELECT * FROM not_registered_customer WHERE email = '$email'";
        $result = $this->db->query($query);
        if (count($result) > 0) {
            return CUSTOMER_NOT_REGISTERD;
        }

        return CUSTOMER_NEW;
    }

    public function isExistInAllCustomer($email, $taxNumber = '') {
        $result = array();
        $query = "SELECT * FROM all_customer WHERE email = '$email' AND tax_number = '$taxNumber'";
        $result = $this->db->query($query);
        if (count($result) > 0) {
            return $result[0];
        } else {
            return false;
        }
    }

    public function isRegisteredCustomer($email, $taxNumber) {
        $data = $this->isExistInAllCustomer($email, $taxNumber);
        if ($data != false) {
            $em_customer_id = $data["em_customer_id"];
            $customerSOAP = \euromedic\CustomerSOAP::create();
            $type = $customerSOAP->getType($em_customer_id);
            $result = false;
            switch ($type) {
                case "1":
                case "2":
                    $query = "SELECT * FROM em_customer_xml WHERE em_customer_id = '$em_customer_id' AND status = 1";
                    if ($this->db->isExist($query)) {
                        $result = true;
                    }
                    break;
            }
            return $result;
        }

        return false;
    }

    public function getCustomerEMIdByEmail($email, $taxNumber) {
        $data = $this->isExistInAllCustomer($email, $taxNumber);
        return $data["em_customer_id"];
    }

    public function updateNotRegistedCustomer($mail) {
        $query = "DELETE FROM not_registered_customer WHERE email = '$email'";
        $this->db->query($query);

        $query = "UPDATE all_customer 
                  SET not_registered_customer = 0
                  WHERE email = '$email'    
                            ";
        $this->db->query($query);
    }

    public function deleteCustomer($list) {

        $deleteResource = \helpers\DeleteResource::create();
        $deleteResource->setIsDelete(true);
        $deleteResource->setList($list);
        $deleteResource->deleteList();
    }

    public function insertCustomer() {
        $request = \resources\Request::create();
        $customerGroup = \resources\CustomerGroup::create();
//        $customerResource = \resources\Customer::create();
        foreach ($this->customers as $customer) {
            $customerResource = \resources\Customer::create();

            $customerId = $customer[CUSTOMER_ID];

            $email = $customer["email"];
            $customerResource->id = $customerId;
            $customerResource->innerId = $customerId;
            $customerResource->email = $email;

            $customerResource->password = randomPassword();


            $customerResource->status = 1;
            $customerResource->approved = 1;


            $customerResource->newsletter = $customer["newsletter"];


//            $customerGroupId = $customerGroup->createOuterId($customer[CUSTOMER_GROUP_ID]);
            $customerGroupId = SR_DEFAULT_CUSTOMER_GROUP;
            $customerResource->customerGroup = array("id" => $customerGroupId);




            $customerResource->firstname = $customer["firstname"];
            $customerResource->lastname = $customer["lastname"];
            $customerResource->telephone = $customer["telephone"];

//            sout($customerResource->getData());
//            $customerResource->insert(true);
            $customerResource->setPathInsert();
            $request->addBatch($customerResource->createBatchArray());
        }

//        sout(($request->getRequest()));
//        die();
        $request->run(true);
    }

    public function insertCustomerAddress() {
        $request = \resources\Request::create();
        $customerResource = \resources\Customer::create();
        $countryResource = \resources\Country::create();
        foreach ($this->customers as $customer) {

            $customerId = $customer[CUSTOMER_ID];
            $customerSRId = $customerResource->createSRId($customerId);

            foreach ($customer["addresses"] as $address) {

                $addressResource = \resources\Address::create();
                $addressResource->firstname = $address["firstname"];
                $addressResource->lastname = $address["lastname"];
                $addressResource->company = $address["company"];
                $addressResource->address1 = $address["address_1"];
                $addressResource->address2 = $address["address_2"];
                $addressResource->postcode = $address["postcode"];
                $addressResource->city = $address["city"];
                $addressResource->customer = array("id" => $customerSRId);
                $addressResource->country = array("id" => $countryResource->getSRCountryIdByIsoCode2($address["isoCode2"]));
//                $addressResource->taxNumber = $address[""];

                $addressResource->setPathInsert();
                $request->addBatch($addressResource->createBatchArray());
            }
        }

//sout(($request->getRequest()));

        $request->run(true);
    }

    public function insertCustomerGroup() {
        $customerGroupArray = $this->db->query(CUSTOMER_GROUP_QUERY);

        $request = \resources\Request::create();
        foreach ($customerGroupArray as $customerGroupValue) {
            $custumerGroupResource = \resources\CustomerGroup::create();
            $custumerGroupResource->id = $custumerGroupResource->createOuterId($customerGroupValue[CUSTOMER_GROUP_ID]);
//            $custumerGroupResource->percentDiscount = $customerGroupValue["reduction"];
            $custumerGroupResource->name = $customerGroupValue["name"];

            $custumerGroupResource->setPathUpdate();
            $request->addBatch($custumerGroupResource->createBatchArray());
        }
        $request->run(true);
    }

    public function insertLoyalPoint() {
        $customerResource = \resources\Customer::create();
        $request = \resources\Request::create();
        foreach ($this->customers as $customer) {
            $customerId = $customer[CUSTOMER_ID];
            $customerSRId = $customerResource->createSRId($customerId);

            $query = " SELECT * FROM gomb_alpha_userpoints WHERE userid = " . $customer['id'];
            $result = $this->db->query($query);
            if (count($result) > 0) {
                $loyaltyPoint = \resources\LoyaltyPoint::create();
                $loyaltyPoint->value = $result[0]["points"];
                $loyaltyPoint->customer = array("id" => $customerSRId);

//                $loyaltyPoint->insert(TRUE);
                $loyaltyPoint->setPathInsert();
                $request->addBatch($loyaltyPoint->createBatchArray());
            }
        }

        $request->run(true);
    }

}
