<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace helpers;

/**
 * Description of CategoryHelper
 *
 * @author tamas
 */
class CategoryHelper extends Helper {

    protected $categories;

    function getCategories() {
        return $this->categories;
    }

    function setCategories($debug = false) {
        $this->categories = $this->db->query(CATEGORY_QUERY);

        foreach ($this->categories as &$category) {
            $categoryId = $category[CATEGORY_ID];
            $query = "SELECT * FROM oc_url_alias WHERE query = 'category_id=$categoryId'";

            $slug = "";
            if ($this->db->isExist($query)) {
                $urlAliasArray = $this->db->query($query);
                $slug = $urlAliasArray[0]["keyword"];
            }
            $category["slug"] = $slug;


            $image = $category["image"];
            $array = explode("/", $image);
            $firstElemet = reset($array);
            if ($firstElemet == "data") {
                $image = substr($image, 5);
            }
            $image = "images/" . $image;
            $category["picture"] = $image;
        }

        if ($debug) {
            sout($this->categories);
            die();
        }
    }

    public function __construct() {
        parent::__construct();
    }

    public static function create() {
        $categoryHelper = new CategoryHelper();
        return $categoryHelper;
    }

    public function deleteCategory($list) {

        $deleteResource = \helpers\DeleteResource::create();
        $deleteResource->setIsDelete(true);
        $deleteResource->setList($list);
        $deleteResource->deleteList();
    }

    public function insertCategory() {
        $request = \resources\Request::create();
        for ($i = 0; $i < count($this->categories); $i++) {
            $category = $this->categories[$i];

            $categoryId = $category[CATEGORY_ID];

            $categoryResource = \resources\Category::create();

            $categoryResource->id = $categoryId;
            $categoryResource->innerId = $categoryId;


            $categoryResource->status = $category["status"];
            $categoryResource->sortOrder = $category["sort_order"];


            $categoryResource->picture = $category["picture"];
//
//            sout($categoryResource->getData());
            $categoryResource->setPathInsert();
//            $categoryResource->insert(true);
            $request->addBatch($categoryResource->createBatchArray());
        }

//sout(($request->getRequest()));
        $request->run(true);
    }

    public function updateCategoryImage() {
        for ($i = 0; $i < count($this->categories); $i++) {
            $category = $this->categories[$i];

            $categoryId = $category[CATEGORY_ID];
            $categorySRId = base64_encode("category-category_id=" . $categoryId);

            $categoryResource = \resources\Category::create();

            $categoryResource->id = $categorySRId;

            $query = "SELECT guid FROM wp_posts WHERE ID =  
                            (SELECT meta_value FROM wp_termmeta 
                             WHERE meta_key = 'thumbnail_id' AND term_id = $categoryId)";
            $picture = $this->db->findOne($query, "guid");
            $categoryResource->picture = str_replace(SHOP_IMAGE_BASE_URL, "", $picture);

            $categoryResource->update(true);
        }
    }

    public function insertCategoryDescription() {
        $request = \resources\Request::create();
        $categoryResource = \resources\Category::create();
        for ($i = 0; $i < count($this->categories); $i++) {
            $category = $this->categories[$i];

            $categoryId = $category[CATEGORY_ID];
            $categorySRId = $categoryResource->createSRId($categoryId);

            $categoryDescriptionResource = \resources\CategoryDescription::create();


            $categoryDescriptionResource->name = $category["name"];

            $categoryDescriptionResource->category = array("id" => $categorySRId);
            $categoryDescriptionResource->language = array("id" => LANG_HU);
            $categoryDescriptionResource->metaDescription = $category["meta_description"];
            $categoryDescriptionResource->metaKeywords = $category["meta_keyword"];
            $categoryDescriptionResource->description = $category["description"];
//            $categoryDescriptionResource->shortDescription = "";

            $categoryDescriptionResource->setPathInsert();
            $request->addBatch($categoryDescriptionResource->createBatchArray());
        }

//sout(($request->getRequest()));
        $request->run(true);
    }

    public function setDescription($category, &$description) {
        $description = $category["category_description"];

        if ($description != NULL || $description != "") {
            $description .= "<br><br>";
        }

        $descArray = array(
            array("gyarto" => "Gyártó"),
            array("fonalvastagsag" => "Fonalvastagság"),
            array("szalhosszusag" => "Szálhosszúság"),
            array("osszetetel" => "Összetétel"),
            array("akcios" => "Akciós"),
            array("trend" => "trend"),
            array("ajanlott_kototumeret" => "Ajánlott kötőtű méret"),
            array("ajanlott_horgolotumeret" => "Ajánlott horgolótű méret"),
            array("pontos_osszetetel" => "Pontos összetétel"),
            array("pontos_szalhosszusag" => "Pontos hosszúság"),
        );

        foreach ($descArray as $descAttr) {

            foreach ($descAttr as $key => $value) {

                $this->addDescriptionValue($category, $key, $value, $description);
            }
        }
    }

    public function addDescriptionValue($category, $key, $keyValue, &$description) {

        if ($category[$key] != NULL || $category[$key] != "") {
            $description .= "$keyValue: " . $category[$key] . "<br><br>";
        }
    }

    public function insertCategoryUrlAlias() {
        $request = \resources\Request::create();
        $categoryResource = \resources\Category::create();
        for ($i = 0; $i < count($this->categories); $i++) {
            $category = $this->categories[$i];

            $categoryId = $category[CATEGORY_ID];
            $categorySRId = $categoryResource->createSRId($categoryId);

            $urlAliasResource = \resources\URLAlias::create();
            $urlAliasResource->type = "CATEGORY";
            $urlAliasResource->urlAliasEntity = array("id" => $categorySRId);

            $alias = slug($category["name"]) . "-" . $categoryId;

            $urlAliasResource->urlAlias = $alias;

            $urlAliasResource->setPathInsert();
            $request->addBatch($urlAliasResource->createBatchArray());
        }
        $request->run(true);
    }

    public function updateCategoryParent() {
        $request = \resources\Request::create();
        $categoryResource = \resources\Category::create();
        for ($i = 0; $i < count($this->categories); $i++) {
            $category = $this->categories[$i];
            $categoryResource = \resources\Category::create();

            $childId = $categoryResource->createSRId($category[CATEGORY_ID]);
            $parentId = $categoryResource->createSRId($category[CATEGORY_PARENT_ID]);

            $categoryResource->id = $childId;
            $categoryResource->parentCategory = array("id" => $parentId);

            if ($category[CATEGORY_PARENT_ID] != "0") {
                $categoryResource->setPathUpdate();
                $request->addBatch($categoryResource->createBatchArray());
            }
        }
        $request->run(true);
    }

}

class AlreadyWas {

    public $name;
    public $count;

    protected function __construct() {
        ;
    }

    public static function create() {
        $aw = new AlreadyWas();
        return $aw;
    }

}

class Voltmar {

    public $array;

    protected function __construct() {
        $this->array = array();
    }

    public static function create() {
        $voltmar = new Voltmar();
        return $voltmar;
    }

    public function add($name) {
        foreach ($this->array as &$value) {
            if ($this->isInArray($name)) {
                $value->count++;
            } else {
                $aw = AlreadyWas::create();
                $aw->name = $name;
                $aw->count = 1;
                $this->array[] = $aw;
            }
        }

//        sout($this->array);
    }

    private function isInArray($name) {
        foreach ($this->array as $value) {
            if ($value == $name) {
                return true;
            }
        }

        return false;
    }

    public function getCountByName($name) {
        foreach ($this->array as $value) {
            if ($value->name === $name) {
                return $value->count;
            }
        }
    }

}
