<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ArtExportUrlHelper
 *
 * @author szekus
 */
class UrlHelperCategory {

    private $links;
    private $arr;
    private $counter;
    private $array;
    private $catageroyId;
    private $refs = array();
    private $list = array();

    public function __construct() {
        $this->links = "";
        $this->counter = 0;
        $this->array = array();
    }

    function createArray($array, $level = 0) {
        foreach ($array as $value) {

            if ($this->counter == $level) {
                $this->links = $this->removeLastElemt($this->links);
            }
            $number = ($this->getLinksLength($this->links) - $level);
            if ($number > 0) {
                $this->links = $this->removeMoreElements($this->links, $number);
            }
            $this->links .= "/" . $value['slug'];


            $this->array[] = new Category($value['id'], $this->links);
            if (!empty($value['children'])) {
                $this->createArray($value['children'], $level + 1);
            }
            $this->counter = $level;
        }
    }

    public function getLinks() {
        return $this->links;
    }

    public function getArray() {
        return $this->array;
    }

    public function getList() {
        return $this->list;
    }

    public function removeLastElemt($param) {
        $urlArray = explode('/', $param);
        $urlArray = array_filter($urlArray);

        array_pop($urlArray);

        $urlString = implode('/', $urlArray);
        if (!empty($urlString)) {
            if ($urlString[0] == "/") {
                return $urlString;
            } else {
                return "/" . $urlString;
            }
        }
    }

    public function getLinksLength($link) {
        $arr = explode("/", $link);
        return count($arr) - 1;
    }

    public function removeMoreElements($link, $number) {

        $urlArray = explode('/', $link);
        $urlArray = array_filter($urlArray);

        for ($i = 0; $i < $number; $i++) {
            array_pop($urlArray);
        }

        $urlString = implode('/', $urlArray);
//        echo $number;
//        echo '<br>';
        if (!empty($urlString)) {
            if ($urlString[0] == "/") {
                return $urlString;
            } else {
                return "/" . $urlString;
            }
        }
    }

    public function createNewUrlArray($array) {
        foreach ($array as $row) {
            $ref = & $this->refs[$row['id']];

            $ref['parent_id'] = $row['parent_id'];

            $shortName = $this->getUrlAliasById($row['id']);
            $ref['slug'] = $shortName;
            $ref['id'] = $row['id'];

            if ($row['parent_id'] == 0) {
                $this->list[$row['id']] = & $ref;
            } else {
                $this->refs[$row['parent_id']]['children'][$row['id']] = & $ref;
            }
        }
    }

    public function getUrlAliasById($urlAliasId) {
        $aliasId = base64_encode("category-category_id=" . $urlAliasId);
        $param = "?categoryId=" . $aliasId . "&type=CATEGORY";
        $result = querySRApi("/urlAliases" . $param, [], 'GET', "responseBody", false);


        $id = (getId($result["items"][0]["href"]));

        $resultAlias = querySRApi("/urlAliases/" . $id, [], 'GET', "responseBody", false);

        return $resultAlias["urlAlias"];
    }

}

class Category {

    private $categoryId;
    private $url;

    public function __construct($categoryId, $url) {
        $this->categoryId = $categoryId;
        $this->url = $url;
    }

    public function __toString() {
        return "catageryId: " . $this->categoryId . "\n url: " . $this->url . "\n";
    }

    public function getCategoryId() {
        return $this->categoryId;
    }

    public function getUrl() {
        $this->url = substr($this->url, 1);
        return $this->url;
    }

}
