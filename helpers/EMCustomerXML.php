<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace helpers;

/**
 * Description of EMCustomerXML
 *
 * @author szekus
 */
class EMCustomerXML extends Helper {

    protected $id;
    protected $em_customer_id;
    protected $status;
    protected $request_xml;
    protected $response_xml;
    protected $last_date_updated;

    protected function __construct() {
        parent::__construct();
    }

    public static function create() {
        $emCustomerXML = new EMCustomerXML();
        return $emCustomerXML;
    }

    public function insert() {

        $query = "SELECT * FROM em_customer_xml WHERE em_customer_id = '$this->em_customer_id'";
        $result = $this->db->query($query);
        if (count($result) > 0) {
            $this->update();
        } else {
            $query = "INSERT INTO em_customer_xml (`id`, `em_customer_id`, `status`, `request_xml`,last_date_updated) 
                  VALUES(null, $this->em_customer_id,0,'$this->request_xml','$this->last_date_updated')";

            $this->db->query($query);
        }
    }

    public function getAll() {
        $query = "SELECT * FROM em_customer_xml";
        $result = $this->db->query($query);

        $resultArray = array();
        foreach ($result as $value) {
            $emCustomerXML = EMCustomerXML::create();
            $emCustomerXML->setId($value["id"]);
            $emCustomerXML->setEm_customer_id($value["em_customer_id"]);
            $emCustomerXML->setRequest_xml($value["request_xml"]);
            $emCustomerXML->setResponse_xml($value["response_xml"]);
            $emCustomerXML->setStatus($value["status"]);
            $emCustomerXML->setLast_date_updated("last_date_updated");

            $resultArray[] = $emCustomerXML;
        }

        return $resultArray;
    }

    public function update($where = null) {
        
        $query = "
            UPDATE em_customer_xml 
            SET em_customer_id=$this->em_customer_id,
                request_xml='$this->request_xml',
                response_xml='$this->response_xml',
                last_date_updated = '$this->last_date_updated', 
                status=$this->status
            WHERE em_customer_id = $this->em_customer_id                    
                ";
//        sout($query);

        $this->db->query($query);
    }

    function getStatus() {
        return $this->status;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function getRequest_xml() {
        return $this->request_xml;
    }

    function setRequest_xml($request_xml) {
        $this->request_xml = $request_xml;
    }

    function getResponse_xml() {
        return $this->response_xml;
    }

    function setResponse_xml($response_xml) {
        $this->response_xml = $response_xml;
    }

    function getEm_customer_id() {
        return $this->em_customer_id;
    }

    function setEm_customer_id($em_customer_id) {
        $this->em_customer_id = $em_customer_id;
    }

    function getId($id = null) {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function getLast_date_updated() {
        return $this->last_date_updated;
    }

    function setLast_date_updated($last_date_updated) {
        $this->last_date_updated = $last_date_updated;
    }

}
