<?php

ini_set('memory_limit', '-1');

function setParentProducts($products, $childProducts) {
    $parentIdArray = array();

    foreach ($products as $value) {
        if ($value["Parent ID"] != "") {
            $parentIdArray[] = $value["Parent ID"];
        }
    }

    $uniques = array_values(array_unique($parentIdArray, SORT_REGULAR));
    $parentProducts = array();
    foreach ($uniques as $unique) {
        foreach ($products as $value) {
            if ($value["Product ID"] == $unique) {
                $parentProducts[] = $value;
            }
        }
    }


    return $parentProducts;
}

function setChildProducts($products) {
    $childProducts = array();
    for ($i = 0; $i < count($products); $i++) {
        $value = $products[$i];

        if ($value["Parent ID"] != "") {
            $childProducts[] = $value;
        }
    }


    return $childProducts;
}

function setNames(&$products) {
    foreach ($products as &$value) {
//        if ($value["Product ID"] == 4294) {

        $names = explode("|", $value["Product Name"]);
        if (count($names) > 1) {
            $nameWithVariationNumberAndManufacturer = $names[0];
            $productName = $names[1];


            $parts = explode(" ", trim($nameWithVariationNumberAndManufacturer));
            $name = "";
            $pineszet = "";
            foreach ($parts as $part) {
                $pineszet = "";
                $start = 0;
                if ($part[0] == "#") {
                    $start = 2;
                } else if ($part == "Variation") {
                    $start = 3;
                } else {
                    $start = 0;
                }
                for ($i = $start; $i < count($parts); $i++) {
                    $pineszet .= $parts[$i] . " ";
                }
                break;
            }
            $name = $pineszet . $productName;
//            sout($name);
            $name = str_replace("  ", " ", $name);
            $name = str_replace("terméknek", "", $name);
            $kiszereles = substr($name, -6);
            if ($kiszereles == "palack" || $kiszereles == "karton") {
                $name = substr($name, 0, -15);
            }
//            $name = str_replace("palack", "", $name);
//            $name = str_replace("karton", "", $name);
//            $name = str_replace(html_entity_decode('&ndash;', ENT_COMPAT, 'UTF-8'), "", $name);
//            $name = str_replace(html_entity_decode('&mdash;', ENT_COMPAT, 'UTF-8'), "", $name);
//            $en_dash = html_entity_decode('&#x2013;', ENT_COMPAT, 'UTF-8');
//            $em_dash = html_entity_decode('\u2014;', ENT_COMPAT, 'UTF-8');
//            $name = str_replace($en_dash, "", $name);
//            $name = str_replace($em_dash, "", $name);
//            sout($name . " " . $value["Product ID"]);
            $value["name"] = trim($name);
        } else {
            $value["name"] = $value["Product Name"];
        }

//        }
    }
}

function setImagesAndPrice(&$products) {
    $database = \db\Database::instance();
    $tmpProducts = array();

    foreach ($products as $value) {
        $tmpProducts[] = $value;
    }


    foreach ($products as &$value) {
        $images = array();
        $galleryIds = explode("|", $value["Product Gallery"]);
        foreach ($galleryIds as $galleryId) {

            if ($galleryId != "") {

                $query = "SELECT meta_value FROM wp_postmeta WHERE meta_key = '_wp_attached_file' AND post_id = " . $galleryId;
                $url = "uploads/" . $database->findOne($query, "meta_value");

                $images[] = $url;
//                sout($images);
            }
        }

//        sout($images);
        $value["images"] = $images;
    }


    foreach ($products as &$value) {
        if ($value["Featured Image"] == "") {
            if ($value["Parent ID"] != "") {
                foreach ($tmpProducts as $tmpProd) {
                    if ($tmpProd["Product ID"] == $value["Parent ID"] && $value["Featured Image"] == "") {
                        $value["Featured Image"] = $tmpProd["Featured Image"];
                    }
                }
            }
        }
    }
    foreach ($products as &$value) {
        if ($value["Price"] == "") {
            if ($value["Parent ID"] != "") {
                foreach ($tmpProducts as $tmpProd) {
                    if ($tmpProd["Product ID"] == $value["Parent ID"] && $value["Featured Image"] == "") {
                        $value["Price"] = $tmpProd["Price"];
                    }
                }
            }
        }
    }
}

function setCategories(&$products) {


    $categoryArray = getCategoryArray();
    $manufacturerArray = getManufacturerArray();



    foreach ($products as &$value) {
        $productCategoryUrlArray = explode("|", $value["Category"]);
        $categoryIds = array();
        $manufacturerIds = array();
        foreach ($productCategoryUrlArray as $productCategoryUrl) {
            $productCategoryNameArray = explode(">", $productCategoryUrl);
            $lastElemet = count($productCategoryNameArray) - 1;
            $theChosenOneCategoryName = $productCategoryNameArray[$lastElemet];

            foreach ($categoryArray as $categoryItem) {
                if ($categoryItem["name"] == $theChosenOneCategoryName) {
                    $isManu = false;
                    foreach ($manufacturerArray as $manufacturerItem) {
                        if ($theChosenOneCategoryName == $manufacturerItem["name"]) {
                            $manufacturerIds[] = $manufacturerItem["id"];
                            $isManu = true;
                        }
                    }
                    if (!$isManu) {
                        $categoryIds[] = $categoryItem["id"];
                    }
                }
            }
        }
        $value["categoryId"] = $categoryIds;
        $value["manufacturerId"] = $manufacturerIds;
    }
}

function getCategoryArray() {
    $categoryResource = resources\Category::create();
    $categories = $categoryResource->getAllFromDB();
    $categoryDescriptionResource = \resources\CategoryDescription::create();
    $categoryDescriptionArray = $categoryDescriptionResource->getAllFromDB();


    $categoryArray = array();
    foreach ($categories as $category) {
//    sout($category->id);
        foreach ($categoryDescriptionArray as $categoryDescription) {
            $categoryId = getId($categoryDescription->category);
            if ($categoryId == $category->id) {
                $arr = array();
                $arr["id"] = $category->innerId;
                $arr["name"] = $categoryDescription->name;
                $categoryArray[] = $arr;
            }
        }
    }

    return $categoryArray;
}

function getManufacturerArray() {
    $manufacturerResource = resources\Manufacturer::create();
    $manufacturers = $manufacturerResource->getAllFromDB();



    $manufacturerArray = array();
    foreach ($manufacturers as $manufacturer) {
        $arr = array();
        $arr["id"] = $manufacturer->innerId;
        $arr["name"] = $manufacturer->name;
        $manufacturerArray[] = $arr;
    }

    return $manufacturerArray;
}

function is_in_array($array, $value) {
    if (empty($array)) {
        $array[] = $value;
    }

    foreach ($array as $val) {
        if ($value["Product ID"] != $val["Product ID"]) {
            $array[] = $value;
        }
    }
}
