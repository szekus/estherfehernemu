<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace helpers;

/**
 * Description of OrderStatusHelper
 *
 * @author tamas
 */
class OrderStatusHelper extends Helper {

    protected function __construct() {
        parent::__construct();
    }

    public static function create() {
        $helper = new OrderStatusHelper();
        return $helper;
    }

    public function isAcceptStatus($order) {
        $bool = false;
        //Teljesítve
        if (getId($order["orderStatus"]) == 'b3JkZXJTdGF0dXMtb3JkZXJfc3RhdHVzX2lkPTU=') {
            $bool = true;
        } else {
            $bool = false;
        }
//        sout($bool);
        return $bool;
    }

}
