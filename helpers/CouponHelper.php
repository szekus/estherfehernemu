<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace helpers;

/**
 * Description of CouponHelper
 *
 * @author szekus
 */
class CouponHelper extends Helper {

    protected $coupons;

    protected function __construct() {
        parent::__construct();
    }

    public static function create() {
        $couponHelper = new CouponHelper();
        return $couponHelper;
    }

    public function insertCoupon($debug = false) {
        $request = \resources\Request::create();
        foreach ($this->coupons as $coupon) {
            $couponResource = \resources\Coupon::create();

            $couponId = $coupon[COUPON_ID];
            $couponSRId = $couponResource->createSRId($couponId);

            $couponResource->id = $couponSRId;
            $couponResource->code = $coupon["code"];
            $couponResource->discountType = $coupon["discountType"];

            if ($couponResource->discountType == "FIXED") {
                $couponResource->fixDiscountValue = $coupon["fixDiscountValue"];
            } else {
                $couponResource->percentDiscountValue = $coupon["percentDiscountValue"];
            }
            $couponResource->freeShipping = $coupon["shipping"];
            $couponResource->dateStart = $coupon["date_start"];
            $couponResource->dateEnd = $coupon["date_end"];
            $couponResource->status = $coupon["status"];
            $couponResource->loginRequired = $coupon["logged"];
            $couponResource->totalNumberOfCoupons = $coupon["uses_total"];
            $couponResource->totalNumberOfCouponsPerCustomer = $coupon["uses_customer"];



            $couponResource->setPathUpdate();
            $request->addBatch($couponResource->createBatchArray());
        }
        $request->run($debug);
    }

    public function insertCouponDescription($debug = false) {
        $request = \resources\Request::create();
        foreach ($this->coupons as $coupon) {
            $couponResource = \resources\Coupon::create();
            $couponId = $coupon[COUPON_ID];
            $couponSRId = $couponResource->createSRId($couponId);

            $couponDescriptionResource = \resources\CouponDescription::create();
            $couponDescriptionResource->name = $coupon["name"];
            $couponDescriptionResource->description = "";
            $couponDescriptionResource->coupon = array("id" => $couponSRId);
            $couponDescriptionResource->language = array("id" => LANG_HU);

            $couponDescriptionResource->setPathInsert();
            $request->addBatch($couponDescriptionResource->createBatchArray());
        }
        $request->run($debug);
    }

    public function insertCouponProductRelation($debug = false) {
        $request = \resources\Request::create();
        foreach ($this->coupons as $coupon) {
            $couponResource = \resources\Coupon::create();
            $couponId = $coupon[COUPON_ID];
            $couponSRId = $couponResource->createSRId($couponId);


            $query = "SELECT * FROM uj_coupon_product WHERE coupon_id = $couponId";
            if ($this->db->isExist($query)) {

                $couponProductArray = $this->db->query($query);
                $productId = $couponProductArray[0][PRODUCT_ID];
                $productSRId = \resources\Product::create()->createSRId($productId);
                $couponProductRelationResource = \resources\CouponProductRelation::create();
                $couponProductRelationResource->coupon = array("id" => $couponSRId);
                $couponProductRelationResource->product = array("id" => $productSRId);

                $couponProductRelationResource->setPathInsert();
                $request->addBatch($couponProductRelationResource->createBatchArray());
            }
        }
        $request->run($debug);
    }

    public function insertCouponCategoryRelation($debug = false) {
        $request = \resources\Request::create();
        foreach ($this->coupons as $coupon) {
            $couponResource = \resources\Coupon::create();
            $couponId = $coupon[COUPON_ID];
            $couponSRId = $couponResource->createSRId($couponId);


            $query = "SELECT * FROM oc_coupon_category WHERE coupon_id = $couponId";
            if ($this->db->isExist($query)) {

                $couponCategoryArray = $this->db->query($query);
                $categoryId = $couponCategoryArray[0][CATEGORY_ID];
                $categorySRId = \resources\Category::create()->createSRId($categoryId);

                $couponCategoryRelationResource = \resources\CouponCategoryRelation::create();
                $couponCategoryRelationResource->coupon = array("id" => $couponSRId);
                $couponCategoryRelationResource->category = array("id" => $categorySRId);

                $couponCategoryRelationResource->setPathInsert();
                $request->addBatch($couponCategoryRelationResource->createBatchArray());
            }
        }
        $request->run($debug);
    }

    function getCoupons() {
        return $this->coupons;
    }

    function setCoupons($debug = false) {

        $couponDataSetter = \datasetter\CouponDataSetter::create();
        $couponDataSetter->setData();
        $this->coupons = $couponDataSetter->getData();

        if ($debug) {
            sout($this->coupons, true);
        }
    }

    public function deleteCoupon($list) {
        $deleteResource = \helpers\DeleteResource::create();

        $deleteResource->setList($list);
        $deleteResource->setIsDelete(true);
        $deleteResource->deleteList();
    }

}
