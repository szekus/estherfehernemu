<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace helpers;

/**
 * Description of ImageDownload
 *
 * @author tamas
 */
class ImageDownload {

    public $localDir;
    public $srcURL;
    public $dst;
    public $imageName;
    public $images = array();

    public static function create() {
        $imageDownload = new ImageDownload();
        return $imageDownload;
    }

    private function __construct() {
        ;
    }

    public function createImagesData($images) {
        $this->images = array();
        foreach ($images as $value) {
            $img = array();
            $imgName = $value["image"];
            $img["fileName"] = $imgName;
//            $arr = explode("_", $imgName);
//            $productId = $arr[0];
//
//            $str = (string) $productId;
//            $numberOfCharacter = strlen($str);
//
//            if ($numberOfCharacter == 1) {
//                $dir = $str;
//            } else {
//                $dir = $str[0] . $str[1];
//            }
//            $img["path"] = $dir . "/" . $imgName;
            $img["path"]= $imgName;

            $this->images[] = $img;
        }
    }

    public function download() {
        $i = 0;
        foreach ($this->images as $image) {
            $pathAndFile = $this->dst . $image['fileName'];
            sout($i++);
            createDirectories($pathAndFile);
            $img = file_get_contents($this->srcURL . $image["path"]);
            file_put_contents($pathAndFile, $img);
        }
    }

}
