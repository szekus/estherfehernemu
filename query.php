<?php

define("COUPON_ID", "coupon_id");
$coupon_query = " 
        SELECT * FROM  oc_coupon
                  ";
define("COUPON_QUERY", $coupon_query);

//---------------------------------------------------

define("CUSTOMER_GROUP_ID", "customer_group_id");
$customer_group_query = " 
        SELECT * FROM oc_customer_group_description
                  ";
define("CUSTOMER_GROUP_QUERY", $customer_group_query);

//---------------------------------------------------

define("CUSTOMER_ID", "customer_id");
$customer_query = " 
        SELECT * FROM oc_customer
        ORDER BY date_added DESC
                  ";
define("CUSTOMER_QUERY", $customer_query);

//---------------------------------------------------


define("MANUFACTURER_ID", "manufacturer_id");
$manufacturer_query = " 
        SELECT * FROM  oc_manufacturer AS m
        ";
define("MANUFACTURER_QUERY", $manufacturer_query);

//---------------------------------------------------

define("CATEGORY_PARENT_ID", "parent_id");
define("CATEGORY_ID", 'category_id');
$category_query = "
    SELECT * FROM oc_category AS c
    INNER JOIN oc_category_description AS cd ON c.category_id = cd.category_id
    WHERE cd.language_id = 2
    ";
define("CATEGORY_QUERY", $category_query);

//---------------------------------------------------

define("PRODUCT_PARENT_ID", "parent");
define('PRODUCT_ID', "product_id");
$product_query = "
        SELECT * FROM oc_product AS p
        INNER JOIN oc_product_description AS pd ON p.product_id = pd.product_id
        WHERE pd.language_id = 2
                 ";
define("PRODUCT_QUERY", $product_query);

//---------------------------------------------------

define("LIST_ATTRIBUTE_ID", "attribute_id");
$list_attribute_query = " 
        SELECT  * FROM uj_attribute_description
        ";

define("LIST_ATTRIBUTE_QUERY", $list_attribute_query);

define("PRODUCT_CLASS_ID", "id");
$product_class_query = " 
        SELECT * FROM products WHERE id IN (SELECT product_id FROM product_versions)
        ";

define("PRODUCT_CLASS_QUERY", $product_class_query);
